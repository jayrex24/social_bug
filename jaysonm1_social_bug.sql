-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 01, 2016 at 08:48 AM
-- Server version: 5.5.52-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `social_bug`
--

-- --------------------------------------------------------

--
-- Table structure for table `bugtype`
--

CREATE TABLE IF NOT EXISTS `bugtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bugtype` varchar(50) CHARACTER SET latin1 NOT NULL,
  `description` varchar(150) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `bugtype`
--

INSERT INTO `bugtype` (`id`, `bugtype`, `description`) VALUES
  (1, 'Fire Ant', 'A problem-solver, always looking for things to improve upon with innovative ideas.'),
  (2, 'Cockroach', 'Creative with incredible intelligence, shocks the world with amazing theories and logic.'),
  (3, 'Bumble Bee', 'A natural born leader, using determination and sharp mind to achieve whatever they seek.'),
  (4, 'Grasshopper', 'Attempts to break every barrier and obstacle to fulfill deep purpose or ultimate end goal.'),
  (5, 'Hercules Beetle', 'Does everything in their power to help others with inborn idealism and sense of morality.'),
  (6, 'Daddy Longlegs', 'May be perceived as calm or even shy, but find ways to make things better at the risk of being misunderstood.'),
  (7, 'Dragonfly', 'A natural born leader with passion and charisma, guiding others and the community to work together.'),
  (8, 'Cricket', 'Free spirit, charming, independent, energetic, compassionate, and the life of the party.'),
  (9, 'Walkingstick', 'Possess integrity, dedication, and practical logic that are the core for any business or family.'),
  (10, 'Wasp', 'Possess integrity, dedication, and practical logic that are the core for any business or family.'),
  (11, 'Ladybug', 'Embraces the values of honesty, dedication, and dignity, takes pride in bringing people together and leading them on difficult paths.'),
  (12, 'Praying Mantis', 'The popular one, enjoys planning social gatherings and making sure everyone has a good time.'),
  (13, 'Black Widow', 'An explorer, examining everything with rationalism and pure curiosity, not afraid to get their hands dirty.'),
  (14, 'Monarch Butterfly', 'True artist, pushing the limits with actions, and challenging traditional expectations with beauty and behavior.'),
  (15, 'Luna Moth', 'Loves to be the center of attention, enjoying making people laugh by entertaining them with their blunt humor.'),
  (16, 'Fly', 'Caught up in the excitement of the moment, and devotes their time and energy to encourage others in a pleasant style.');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `user_id`, `ip_address`, `timestamp`) VALUES
  (1, 1, '192.168.0.1', '2016-12-01 08:43:36'),
  (2, 1, '192.168.0.1', '2016-12-01 08:45:12'),
  (3, 1, '192.168.0.1', '2016-12-01 08:47:07');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE IF NOT EXISTS `followers` (
  `user_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msg_messages`
--

CREATE TABLE IF NOT EXISTS `msg_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '0',
  `sender_id` int(11) NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `msg_participants`
--

CREATE TABLE IF NOT EXISTS `msg_participants` (
  `user_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msg_status`
--

CREATE TABLE IF NOT EXISTS `msg_status` (
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`message_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `msg_threads`
--

CREATE TABLE IF NOT EXISTS `msg_threads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id`, `question`) VALUES
  (1, 'You find it easy to introduce yourself to other people.'),
  (2, 'A logical decision is always the best, even when it hurts someone''s feelings.'),
  (3, 'You try to respond to your e-mails as soon as possible and cannot stand a messy inbox.'),
  (4, 'You don''t usually initiate conversations.'),
  (5, 'You find it easy to stay relaxed and focused even when there is some pressure.'),
  (6, 'You often get so lost in thoughts that you ignore or forget your surroundings.'),
  (7, 'Deadlines do not stress you out.'),
  (8, 'Being adaptable is more important to you than being organized.'),
  (9, 'Emotional movies can easily make you sad.'),
  (10, 'Your home and work environments are quite tidy.'),
  (11, 'You often feel as if you have to justify yourself to other people.'),
  (12, 'You like to leave matters undecided for as long as possible as there may be better options later.'),
  (13, 'You do not mind being at the center of attention.'),
  (14, 'You would rather call yourself pragmatic than visionary.'),
  (15, 'You rarely get mood swings.'),
  (16, 'Your travel plans are usually well thought out.'),
  (17, 'You usually think a lot before you speak.'),
  (18, 'Your mood can change very quickly.'),
  (19, 'You are more of a natural improviser than a careful planner.'),
  (20, 'You would rather have your work reflect your values than your reasoning skills.'),
  (21, 'Your work style is closer to random energy spikes than to a methodical and organized approach.'),
  (22, 'You are often envious of others.'),
  (23, 'An interesting book or a video game is often better than a social event.'),
  (24, 'Being able to develop a plan and stick to it is the most important part of every project.'),
  (25, 'Ideas are often more important than real things.'),
  (26, 'When there is trouble, you start by offering emotional support rather than practical solutions.'),
  (27, 'If someone does not respond to your e-mail quickly, you start worrying if you said something wrong.'),
  (28, 'You do not let your emotions show, even when you are with close friends.'),
  (29, 'It is a challenge for you to look at criticism objectively.'),
  (30, 'Your dreams tend to be vivid and focus on a fictional world.'),
  (31, 'It does not take you much time to start getting involved in social activities at your new workplace.'),
  (32, 'You are better at implementing than innovating.'),
  (33, 'It is easy to irritate you.'),
  (34, 'You worry too much about what other people think.'),
  (35, 'You often spend time exploring unrealistic and impractical yet intriguing ideas.'),
  (36, 'You do not mind being criticized, even harshly, if the arguments make sense.'),
  (37, 'When solving problems, you look for tested methods instead of experimenting with novel ideas.'),
  (38, 'You would rather improvise than spend time coming up with a detailed plan.'),
  (39, 'You often spend more time reflecting on things than actually doing them.'),
  (40, 'You believe that moral positions are often more important than what makes logical sense.'),
  (41, 'Keeping your options open is more important than having a to-do list.'),
  (42, 'You would rather work in a team than alone.'),
  (43, 'You rarely feel insecure.'),
  (44, 'You have no difficulties coming up with a personal timetable and sticking to it.'),
  (45, 'Being efficient is more important than being cooperative when it comes to teamwork.'),
  (46, 'When making plans, you often get lost in possibilities and forget what is realistic.'),
  (47, 'You feel more energetic after spending time with a group of people.'),
  (48, 'When making decisions affecting others, you would rather be seen as illogical than as unsympathetic.'),
  (49, 'You see yourself as very emotionally stable.'),
  (50, 'You cannot stand chaos.'),
  (51, 'You are more traditional than unconventional.'),
  (52, 'You prefer deeper rather than more frequent conversations.'),
  (53, 'It is difficult for you to hide your feelings.'),
  (54, 'You are a relatively reserved and quiet person.'),
  (55, 'If the room is full, you stay closer to the walls, avoiding the center.'),
  (56, 'Politically, you are more conservative than liberal.'),
  (57, 'You feel very anxious in stressful situations.'),
  (58, 'You focus on the possibilities rather than the realities.'),
  (59, 'You think that there is little point in being empathetic in a professional environment.'),
  (60, 'You often take initiative in social situations.');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `user_id` int(11) NOT NULL,
  `token` varchar(500) CHARACTER SET latin1 NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `birth_date` varchar(20) NOT NULL,
  `address` varchar(250) NOT NULL,
  `state` varchar(100) NOT NULL,
  `zip` varchar(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interests` text NOT NULL,
  `favorite_movies` text NOT NULL,
  `favorite_books` text NOT NULL,
  `bugtype` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_images`
--

CREATE TABLE IF NOT EXISTS `user_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location` varchar(100) CHARACTER SET latin1 NOT NULL,
  `image_name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `image_type` varchar(20) CHARACTER SET latin1 NOT NULL,
  `set_profile` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
