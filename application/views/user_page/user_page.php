<script>
    document.getElementById('links').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };
</script>
<div class="container mtb spacer">
    <div class="row">
        <div class="col-md-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <img src="<?= $profile_pic_path ?>" class="img-thumbnail img-responsive" />
                        <h4 class="panel-title top-buffer" align="center">
                            <?= $user_info->username ?>
                        </h4>
                        <?
                            $follower_count = $this->followers_model->getFollowers($user_info->id);
                            $following_count = $this->followers_model->getFollowing($user_info->id);
                        ?>
                        <p>
                            <span class="label label-warning"><?= count($follower_count) > 1 ? count($follower_count)." followers" : count($follower_count)." follower" ?></span>
                            <span class="label label-info"><?= count($following_count) ?> Following</span>
                        </p>
                        <?
                            if(isset($_SESSION['logged_in'])): ?>
                            <? if($user_info->id == $_SESSION['user_id']){ ?>
                                <h6>Your Profile</h6>
                            <? }elseif(in_array($user_info->id, $following)){ ?>
                                <?= form_open(); ?>
                                <input type="hidden" name="unfollow_id" id="unfollow_id" value="<?= $user_info->id ?>">
                                <button type="submit" class="btn btn-xs btn-danger" name="unfollowUser" id="unfollowUser"><i class="fa fa-minus-circle" aria-hidden="true"></i> Unfollow</button>
                                <?= form_close(); ?>
                            <? } else { ?>
                                <?= form_open(); ?>
                                <input type="hidden" name="follow_id" id="follow_id" value="<?= $user_info->id ?>">
                                <button type="submit" class="btn btn-xs btn-primary" name="followUser" id="followUser"><i class="fa fa-plus-circle" aria-hidden="true"></i> Follow</button>
                                <?= form_close(); ?>
                            <? } ?>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <? if(!empty($bugtype)): ?>
            <div class="panel panel-default" >
                <div class="panel-heading panel-heading-sm">
                    <h5 class="panel-title"><?= sprintf("Bug Type: %s", trim($bugtype['bugtype']))?></h5>
                </div>
                <div class="panel-body">
                    <p><?= sprintf("<h5>Description: </h5><blockquote>%s</blockquote>", trim($bugtype['description']))?></p>
                </div>
            </div>
            <? endif; ?>
        </div>
        <div class="col-md-9 content">
            <div class="panel panel-default" >
                <div class="panel-heading panel-heading-sm">
                    <h5 class="panel-title"><?= sprintf("About %s", trim($user_info->username))?></h5>
                </div>
                <div class="panel-body">
                    <? if(!empty($user_info->interests)|| !empty($user_info->favorite_books) || !empty($user_info->favorite_movies)):
                        echo !empty($user_info->interests) ? "<h5>Interests: </h5><blockquote>".$user_info->interests."</blockquote>" : "";
                        echo !empty($user_info->favorite_movies) ? "<h5>Favorite Movies: </h5><blockquote>".$user_info->favorite_movies."</blockquote>" : "";
                        echo !empty($user_info->favorite_books) ? "<h5>Favorite Books: </h5><blockquote>".$user_info->favorite_books."</blockquote>" : "";
                       else: ?>
                        <h5><?= sprintf("%s has not provided information.", $user_info->username) ?></h5>
                    <? endif; ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-sm">
                    <h5 class="panel-title"><?= sprintf("%s's Pictures", trim($user_info->username))?></h5>
                </div>
                <div class="panel-body">
                    <?php
                    $this->load->helper('directory'); //load directory helper
                    if(!empty($user_photos)) : ?>
                        <div id="links">
                        <? foreach ($user_photos as $k => $photo) : ?>
                            <a data-gallery="" title="<?= $photo['image_name'] ?>" href="<?= $photo['location'] ?>">
                                <img src="<?= $photo['location'] ?>" style="max-height: 80px; max-width: 80px;">
                            </a>
                        <? endforeach; ?>
                        </div>
                    <? else: ?>
                        <h5>No photos uploaded by <?= $user_info->username ?>.</h5>
                    <? endif; ?>
                </div>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <?= $pagination; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body next"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                        Previous
                    </button>
                    <button type="button" class="btn btn-primary next">
                        Next
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
