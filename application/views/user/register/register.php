<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container mtb">
	<!-- Main Form -->
	<div class="col-md-4 col-md-offset-4 spacer">
		<h2>Sign up</h2>
		<?= form_open(); ?>
			<div>
				<div>
					<div class="custom-input">
					  <label for="username">Username</label>
					  <input type="text" class="form-control" id="username" name="username">
					</div>
					<div class="custom-input">
					  <label for="Password">Password</label>
					  <input type="password" class="form-control" id="password" name="password">
					</div>
					<div class="custom-input">
					  <label for="password_confirm">Confirm Password</label>
					  <input type="password" class="form-control" id="password_confirm" name="password_confirm">
					</div>
					<div class="custom-input">
					  <label for="email">Email</label>
					  <input type="email" class="form-control" id="email" name="email">
					  <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					</div>
					<div class="form-group">
						<input type="hidden" class="form-control" id="new" name="new" value="1">
					</div>
				</div>
				<button type="submit" name="submit" value="formsubmit" class="btn btn-primary btn-sm">Register</button>
			</div>
		<?= form_close(); ?>
		<div>
			<p>already have an account? <a href="<?= base_url('user/login') ?>">login here</a></p>
		</div>
		<? if (validation_errors()) : ?>
			<div class="main-login-form">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<? endif; ?>
		<? if (isset($error)) : ?>
			<div class="main-login-form">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<? endif; ?>
	</div>
	<!-- end:Main Form -->
</div>
