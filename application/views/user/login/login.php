<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container mtb">
	<? if($this->session->flashdata('error') || $this->session->flashdata('success')): ?>
		<div class="row">
			<div class="col-md-12">
				<? if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?= $this->session->flashdata('success'); ?>
					</div>
				<? } ?>
				<? if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?= $this->session->flashdata('error'); ?>
					</div>
				<? } ?>
			</div>
		</div>
	<? endif; ?>
	<!-- Main Form -->
	<div class="col-md-4 col-md-offset-4 spacer">
		<?= form_open() ?>
			<h2>Log In</h2>
			<div>
				<div>
					<div class="custom-input">
						<label for="username">Username</label>
						<input type="text" class="form-control" id="username" name="username">
					</div>
					<div class="custom-input">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" name="password">
						<small class="form-text text-muted">Minimum 6 Characters</small>
					</div>
				</div>
				<div class="top-buffer">
					<button type="submit" name="submit" value="formsubmit" class="btn btn-primary btn-sm top">Log in</button>
				</div>
			</div>
		<?= form_close(); ?>
		<div class="etc-login-form top-buffer">
			<a href="<?= base_url('user/forgot_password'); ?>">Forgot Password?</a> <br/>
			new user? <a href="<?= base_url('user/register'); ?>">create new account</a>
		</div>
		<div class="main-login-form top-buffer">
			<? if (validation_errors()) : ?>
				<div class="col-md-12 top">
					<div class="alert alert-danger" role="alert">
						<?= validation_errors() ?>
					</div>
				</div>
			<? endif; ?>
			<? if (isset($error)) : ?>
				<div class="col-md-12">
					<div class="alert alert-danger" role="alert">
						<?= $error ?>
					</div>
				</div>
			<? endif; ?>
		</div>
	</div>
	<!-- end:Main Form -->
</div>