<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container mtb spacer">
    <!-- Main Form -->
    <div class="col-md-4 col-md-offset-4">
        <h2>Reset Password</h2>
        <small class="form-text text-muted"><?= sprintf('Hello %s, Please enter your new password.', $username)?></small>
        <?= form_open(); ?>
            <div class="custom-input">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="custom-input">
                <label for="passconf">Password Confirm</label>
                <input type="password" class="form-control" id="passconf" name="passconf">
                <small class="form-text text-muted">Minimum 6 Characters</small>
            </div>
            <div class="top-buffer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        <?= form_close(); ?>
    </div>
    <div class="col-md-4 col-md-offset-4 mt">
        <? if (validation_errors()) : ?>
            <div class="main-login-form">
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors() ?>
                </div>
            </div>
        <? endif; ?>
        <? if (isset($error)) : ?>
            <div class="main-login-form">
                <div class="alert alert-danger" role="alert">
                    <?= $error ?>
                </div>
            </div>
        <? endif; ?>
        <? if (isset($success)) : ?>
            <div class="main-login-form">
                <div class="alert alert-success" role="alert">
                    <?= $success ?>
                </div>
            </div>
        <? endif; ?>
    </div>
    <!-- end:Main Form -->
</div>