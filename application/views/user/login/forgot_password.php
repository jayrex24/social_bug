<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container mtb spacer">
	<!-- Main Form -->
	<div class="col-md-4 col-md-offset-4">
	<h2>Forgot Password</h2>
	<?= form_open(); ?>
		<div class="custom-input">
			<label for="email">Email address</label>
	  		<input type="text" class="form-control" id="email" name="email">
	  		<small class="form-text text-muted">Start the recovery process by entering your email.</small>
		</div>
		<div class="top-buffer">
			<button type="submit" class="btn btn-primary">Submit</button>
		</div>
	<?= form_close(); ?>
	</div>
	<div class="col-md-4 col-md-offset-4">
		<p>just remembered? <a href="<?= base_url('user/login') ?>">login here</a></p>
	</div>
	<div class="col-md-4 col-md-offset-4 mt">
	<? if (validation_errors()) : ?>
		<div class="main-login-form">
			<div class="alert alert-danger" role="alert">
				<?= validation_errors() ?>
			</div>
		</div>
	<? endif; ?>
	<? if (isset($error)) : ?>
		<div class="main-login-form">
			<div class="alert alert-danger" role="alert">
				<?= $error ?>
			</div>
		</div>
	<? endif; ?>
	<? if (isset($success)) : ?>
		<div class="main-login-form">
			<div class="alert alert-success" role="alert">
				<?= $success ?>
			</div>
		</div>
	<? endif; ?>
	</div>
	<!-- end:Main Form -->
</div>