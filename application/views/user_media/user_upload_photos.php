<div class="panel-body mtb">
    <?= form_open_multipart('',array('class' => 'well')) ?>
        <div class="form-group">
            <label for="file">Select a file to upload</label>
            <input type="file" name="userfile" id="userfile">
            <p class="help-block">Only jpg, jpeg, png and gif file with maximum size of 2 MB is allowed.</p>
        </div>
        <input type="submit" name="upload_photos" id="upload_photos" class="btn btn-sm btn-primary" value="Upload">
        <a class="btn btn-sm btn-info" href="<?= base_url('user_media/view_gallery'); ?>" role="button">View Gallery</a>
    <?= form_close() ?>
</div>

