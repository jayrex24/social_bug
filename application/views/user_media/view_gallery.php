<div class="panel-body">
	<?php
		$this->load->helper('directory'); //load directory helper
		if(!empty($user_photos)) : ?>
			<div id="links">
			<? foreach ($user_photos as $photo) : ?>
					<a title="<?= $photo['image_name'] ?>" href="<?= base_url("user_media/edit_photos/".$_SESSION['user_id']."/".$photo['image_name'])?>">
						<img src="<?= $photo['location'] ?>" alt="<?= $photo['image_name']?>" data-toggle="tooltip" title="Click to edit photo" style="max-width: 80px; max-height: 80px;">
					</a>
			<? endforeach; ?>
			</div>
		<? else: ?>
			<h3>No photos uploaded, just yet.</h3>
			<a class="btn btn-primary" href="<?= base_url('user_media/upload_photos') ?>" role="button">Upload Photos</a>
		<? endif; ?>
	<div class="row">
		<div class="col-md-12" align="center">
			<?= $pagination; ?>
		</div>
	</div>
</div>