<div class="panel-body">
    <div class="row">
        <div class="col-md-3 thumb" data-toggle="tooltip" title="Click to view enlarged">
            <img id="myImg" class="thumbnail" src="<?= $selected_photo['location']; ?>" alt="<?= $selected_photo['image_name'] ?>" onclick="showModal()">
            <div class="desc" style="display: none;"><?= $selected_photo['image_name'] ?></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= form_open() ?>
            <div class="form-group row">
                <label for="image_name" class="col-sm-2 form-control-label">Image Name</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="image_name" name="image_name" value="<?= $selected_photo['image_name'] ?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="image_type" class="col-sm-2 form-control-label">Image Type</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="image_type" name="image_type" value="<?= $selected_photo['image_type'] ?>" readonly>
                </div>
            </div>
            <div class="form-group row">
                <label for="image_desc" class="col-sm-2 form-control-label">Description</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="5" id="image_desc" name="image_desc" placeholder="<?= !empty($selected_photo['description']) ? $selected_photo['description'] : 'Enter Photo Description' ?>" ></textarea>
                </div>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="set_profile" id="set_profile" value="1" <?= $selected_photo['set_profile'] == 1 ? " checked disabled" : "" ?>><strong>Make profile photo</strong></label>
            </div>
            <input type="hidden" name="curr_profile_pic" value="<?= $selected_photo['set_profile'] == 1 ? $selected_photo['set_profile'] : "" ?>">
            <hr>
            <div class="form-group row">
                <div class="col-sm-6">
                    <input type="submit" class="btn btn-primary" name="editphoto" id="editphoto" value="Save">
                    <a href="<?= base_url('user_media/view_gallery')?>" ><button type="button" class="btn btn-secondary" name="cancel">Cancel</button></a>
                    <button type="submit" class="btn btn-danger" name="deletePhoto" onclick="return confirm('Are you sure you want to delete this photo?')" >Delete</button>
                </div>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- The Modal -->
<div id="myModal" class="modal">
    <span class="close">×</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>