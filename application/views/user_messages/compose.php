<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <?= form_open() ?>
                <div class="form-group row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-6">
                        <img class="img-responsive thumbnail" src="<?= $profile ?>" style="max-height: 8em; max-width: 8em;">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-sm-1 control-label">To</label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" value="<?= $message_to ?>" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subject" class="col-sm-1 control-label">Subject</label>
                    <div class="col-sm-11">
                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="message" class="col-sm-12 control-label">Message</label>
                    <div class="col-md-12">
                        <textarea class="form-control" rows="5" name="message" id="message" placeholder="Enter Message"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>