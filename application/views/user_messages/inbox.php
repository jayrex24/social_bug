<?
    $inbox_count = 0;
    $message_unread = 0;
    foreach ($thread as $msg) {
        if ($msg['user_name'] != $_SESSION['username']) {
            $inbox_count++;
            if($msg['status'] == 0){
                $message_unread++;
            }
        }
    }
?>
<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <!-- Tab panes -->
            <p class="text-muted"><?= $inbox_count ?> <?= pluralize($inbox_count , 'Total Message', 'Total Messages') ?> (<?= $message_unread ?> Unread)</p>
            <div class="list-group scroll-inbox">
                    <? if($inbox_count == 0): ?>
                        <a href="#" class="list-group-item">
                            <span class="name" style="min-width: 120px; display: inline-block;"><strong>No messages</strong></span>
                        </a>
                    <? else :?>
                        <?  foreach ($thread as $message): ?>
                            <? if($message['user_name'] != $_SESSION['username']):
                                    $subject = strlen($message['subject']) > 25 ? substr($message['subject'], 0, 25)."..." : $message['subject'];
                                    $body = strlen($message['body']) > 40 ? substr($message['body'], 0, 40)."..." : $message['body'];
                                    $enc_key = $this->encrypt->encode($message['thread_id']);
                                    $enc_key = str_replace(array('+', '/', '='), array('-', '_', '~'), $enc_key);
                            ?> <a href="<?= base_url('pm/read/'.$enc_key) ?>" class="list-group-item <?= $message['status'] == 0 ? 'list-group-item-info' : '' ?>">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <span class="name" style="min-width: 120px; display: inline-block;"><strong><?= $message['user_name'] ?></strong></span>
                                        </div>
                                        <div class="col-md-2">
                                            <span class=""><?= $subject ?></span>
                                        </div>
                                        <div class="col-md-5">
                                            <span class="text-muted" style="font-size: 10px;"> - <?= $body ?></span>
                                        </div>
                                        <div class="col-md-1">
                                            <span class="badge"><?= $message['cdate']?></span>
                                        </div>
                                    </div>
                                </a>
                            <? endif; ?>
                        <? endforeach; ?>
                    <? endif; ?>
                </a>
            </div>
        </div>
    </div>
</div>