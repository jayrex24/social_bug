<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-group scroll-messages">
            <? foreach ($message as $m): ?>
                <div class="list-group-item list-group-item-action <?= $_SESSION['username'] == $m['user_name'] ? 'list-group-item-success' : ''?>">
                    <h7 class="list-group-item-heading col-md-3 pull-left"><span class="text-muted"><small><?= $m['cdate'] ?></small></span></h7>
                    <h5 class="list-group-item-heading col-md-2 pull-right"><span class="text-muted"><strong><?= $m['user_name'] ?></strong></span></h5><br>
                    <p class="list-group-item-text"><?= $m['body'] ?></p>
                </div>
            <? endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="panel-footer">
    <?= form_open() ?>
    <div class="row">
        <div class="col-md-12">
            <label for="reply" class="col-sm-12 control-label">Reply</label>
            <div class="col-sm-12">
                <textarea class="form-control" rows="2" name="reply" id="reply" placeholder="Enter Reply Message" required></textarea>
            </div>
        </div>
    </div>
    <div class="row mt">
        <div class="col-sm-1">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
        </div>
    </div>
    <?= form_close() ?>
</div>