<div class="container mtb spacer">
    <div class="row">
        <div class="text-center">
            <h2>Page Not Found!</h2>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <h3>I think we're lost.</h3>
        </div>
    </div>
    <div class="row">
        <div class="text-center">
             <img src="/assets/images/lost.jpg" class="img-rounded" alt="Lost Directions" width="300" height="200">
        </div>
    </div>
    <div class="row top-buffer">
        <div class="text-center">
            <a class="btn btn-primary" href="<?= base_url() ?>" role="button">Let's head Home</a>
        </div>
    </div>
</div>
