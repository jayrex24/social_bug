<div class="container mtb">
<? if(isset($_GET['newuser'])) { ?>
    <div class="alert alert-success">
        <?= sprintf(_('Welcome <strong>%s</strong>! Make yourself at home.'),  $_GET['newuser']); ?>
    </div>
<? } elseif(isset($success)){ ?>
    <div class="alert alert-success">
        <strong>Success:</strong><br/>
        <? foreach($success as $s){ ?>
            <?= $s ?><br/>
        <? } ?>
    </div>
<? } elseif (validation_errors()) { ?>
    <div class="main-login-form">
        <div class="alert alert-danger" role="alert">
            <strong>Error:</strong>
            <?= validation_errors() ?>
        </div>
    </div>
<? } elseif(isset($upload_data)){ ?>
    <div class="alert alert-success">
        <strong>SUCCESS!</strong> Your Photo <strong> <?= $upload_data['file_name']; ?></strong> has been uploaded!
        <a href="<? base_url('user_profile/view_gallery') ?>">View Here</a>
    </div>
<? } elseif(isset($errors)){ ?>
    <div class="alert alert-danger">
        <strong>Error:</strong><br/>
        <? foreach($errors as $error){ ?>
            <?= $error; ?><br/>
        <? } ?>
    </div>
<? } elseif($this->session->flashdata('message')){ ?>
    <div class="row">
        <div class="col-md-12">
            <? if($this->session->flashdata('message')){ ?>
                <div class="alert alert-info">
                    <?= $this->session->flashdata('message'); ?>
                </div>
            <? } ?>
        </div>
    </div>
<? } ?>
    <div class="row">
        <div class="col-md-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <a href="<?= base_url('user_media/view_gallery')?>"><img src="<?= $profile_pic; ?>" class="img-thumbnail img-responsive" /></a>
                        <h4 class="panel-title top-buffer" align="center">
                            <?= $_SESSION['username'] ?>
                        </h4>
                        <?
                            $follower_count = $this->followers_model->getFollowers($_SESSION['user_id']);
                            $following_count = $this->followers_model->getFollowing($_SESSION['user_id']);
                        ?>
                        <p>
                            <span class="label label-warning"><?= count($follower_count) > 1 ? count($follower_count)." followers" : count($follower_count)." follower" ?></span>
                            <span class="label label-info"><?= count($following_count) ?> Following</span>
                        </p>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-list-alt" aria-hidden="true"></i> USER INFO</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse
                        <? if(!empty($active_menu) && $active_menu == 'User Profile' || $active_menu == 'Update Password' || $active_menu == 'About') echo 'in'; ?>">
                        <div class="list-group">
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'User Profile'    ? 'active' : '' ?>" href="<?= base_url('user_profile/user_home') ?>"><i class="fa fa-user-circle" aria-hidden="true"></i> User Profile</a>
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'Update Password' ? 'active' : '' ?>" href="<?= base_url('user_profile/change_password') ?>"><i class="fa fa-key" aria-hidden="true"></i> Update Password</a>
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'About'           ? 'active' : '' ?>" href="<?= base_url('user_profile/about') ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> About</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        <h4 class="panel-title">
                            <a class="active" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="fa fa-picture-o" aria-hidden="true"></i> MEDIA</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse <? if(!empty($active_menu) && $active_menu == 'View Gallery' || $active_menu == 'Upload Photos' || $active_menu == 'Upload Videos') echo 'in'; ?>">
                        <div class="list-group">
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'View Gallery'  ? 'active' : '' ?>" href="<?= base_url('user_media/view_gallery') ?>"><i class="fa fa-folder-open-o" aria-hidden="true"></i> View Gallery</a>
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'Upload Photos' ? 'active' : '' ?>" href="<?= base_url('user_media/upload_photos') ?>"><i class="fa fa-camera" aria-hidden="true"></i> Upload Photos</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><i class="fa fa-inbox" aria-hidden="true"></i> MESSAGES</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse <? if(!empty($active_menu) && $active_menu == 'Inbox' || $active_menu == 'Sent' || $active_menu == 'Trash') echo 'in'; ?>">
                        <div class="list-group">
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'Inbox' ? 'active' : '' ?>" href="<?= base_url('pm/inbox') ?>"><i class="fa fa-envelope" aria-hidden="true"></i> Inbox<span class="badge badge-info"><?= $message_unread ?></span></a>
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'Sent'  ? 'active' : '' ?>" href="<?= base_url('pm/sent') ?>"><i class="fa fa-paper-plane" aria-hidden="true"></i> Sent</a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><i class="fa fa-star-o" aria-hidden="true"></i> PERSONALITY TEST</a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse <? if(!empty($active_menu) && $active_menu=='Survey') echo 'in'; ?>">
                        <div class="list-group">
                            <a class="list-group-item <?= isset($active_menu) && $active_menu == 'Survey' ? 'active' : '' ?>" href="<?= base_url('user_survey/take_survey') ?>"><i class="fa fa-check-square-o" aria-hidden="true"></i> Survey</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4><?= $active_menu; ?></h4>
                    <? if($active_menu == "View Gallery"){ ?>
                        <strong><small><a href="<?= base_url('user_media/upload_photos')?>">Upload Photos</a></small></strong>
                    <? } ?>
                </div>
			