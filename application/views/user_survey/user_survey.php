<script type="application/javascript">
    $(document).ready(function(){
        var current = 0,current_step,next_step,steps;
        steps = $("fieldset").length - 1;
        $(".next").click(function(){
            $("html, body").animate({ scrollTop: 0 }, 400);
            current_step = $(this).parent();
            next_step = $(this).parent().next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
        });
        $(".previous").click(function(){
            $("html, body").animate({ scrollTop: 0 }, 400);
            current_step = $(this).parent();
            next_step = $(this).parent().prev();
            next_step.show();
            current_step.hide();
            setProgressBar(--current);
        });
        setProgressBar(current);
        // Change progress bar action
        function setProgressBar(curStep){
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width",percent+"%")
                .html(percent+"%");
        }
    });
</script>

<div class="panel-body">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <h4>Progress Bar</h4>
            <div class="progress">
                <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?= form_open('',array('id' => 'survey_form'))?>
                <fieldset>
                    <?= !empty($bugtype) ? sprintf("<span class='label label-info'>Current Bugtype: %s</span>", $bugtype) : '' ?>
                    <h2><?= $greeting ?></h2>
                    <input type="button" name="next" id="next" class="next btn btn-primary mb" value="Start" />
                </fieldset>
                <fieldset>
                    <h2>Survey: Page 1 of 6</h2>
                    <? foreach ($survey[0] as $s): ?>
                        <div class="row">
                            <div class="well">
                                <span class="badge"><?= $s['id'] ?></span>&nbsp;<strong><?= $s['question'] ?></strong>
                                <div class="radio"><label><input type="radio" name="question[<?= $s['id'] ?>]" value="2">Strongly Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s['id'] ?>]" value="1">Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s['id'] ?>]" value="0" checked>Nuetral</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s['id'] ?>]" value="-1">Disagree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s['id'] ?>]" value="-2">Strongly Disagree</label></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                    <input type="button" name="next" id="next" class="next btn btn-info" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Survey: Page 2 of 6</h2>
                    <? foreach ($survey[1] as $s1): ?>
                        <div class="row">
                            <div class="well">
                                <span class="badge"><?= $s1['id'] ?></span>&nbsp;<strong><?= $s1['question'] ?></strong>
                                <div class="radio"><label><input type="radio" name="question[<?= $s1['id'] ?>]" value="2">Strongly Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s1['id'] ?>]" value="1">Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s1['id'] ?>]" value="0" checked>Nuetral</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s1['id'] ?>]" value="-1">Disagree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s1['id'] ?>]" value="-2">Strongly Disagree</label></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                    <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                    <input type="button" name="next" id="next" class="next btn btn-info" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Survey: Page 3 of 6</h2>
                    <? foreach ($survey[2] as $s2): ?>
                        <div class="row">
                            <div class="well">
                                <span class="badge"><?= $s2['id'] ?></span>&nbsp;<strong><?= $s2['question'] ?></strong>
                                <div class="radio"><label><input type="radio" name="question[<?= $s2['id'] ?>]" value="2">Strongly Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s2['id'] ?>]" value="1">Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s2['id'] ?>]" value="0" checked>Nuetral</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s2['id'] ?>]" value="-1">Disagree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s2['id'] ?>]" value="-2">Strongly Disagree</label></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                    <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                    <input type="button" name="next" id="next" class="next btn btn-info" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Survey: Page 4 of 6</h2>
                    <? foreach ($survey[3] as $s3): ?>
                        <div class="row">
                            <div class="well">
                                <span class="badge"><?= $s3['id'] ?></span>&nbsp;<strong><?= $s3['question'] ?></strong>
                                <div class="radio"><label><input type="radio" name="question[<?= $s3['id'] ?>]" value="2">Strongly Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s3['id'] ?>]" value="1">Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s3['id'] ?>]" value="0" checked>Nuetral</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s3['id'] ?>]" value="-1">Disagree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s3['id'] ?>]" value="-2">Strongly Disagree</label></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                    <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                    <input type="button" name="next" id="next" class="next btn btn-info" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Survey: Page 5 of 6</h2>
                    <? foreach ($survey[4] as $s4): ?>
                        <div class="row">
                            <div class="well">
                                <span class="badge"><?= $s4['id'] ?></span>&nbsp;<strong><?= $s4['question'] ?></strong>
                                <div class="radio"><label><input type="radio" name="question[<?= $s4['id'] ?>]" value="2">Strongly Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s4['id'] ?>]" value="1">Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s4['id'] ?>]" value="0" checked>Nuetral</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s4['id'] ?>]" value="-1">Disagree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s4['id'] ?>]" value="-2">Strongly Disagree</label></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                    <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                    <input type="button" name="next" id="next" class="next btn btn-info" value="Next" />
                </fieldset>
                <fieldset>
                    <h2>Survey: Page 6 of 6</h2>
                    <? foreach ($survey[5] as $s5): ?>
                        <div class="row">
                            <div class="well">
                                <span class="badge"><?= $s5['id'] ?></span>&nbsp;<strong><?= $s5['question'] ?></strong>
                                <div class="radio"><label><input type="radio" name="question[<?= $s5['id'] ?>]" value="2">Strongly Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s5['id'] ?>]" value="1">Agree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s5['id'] ?>]" value="0" checked>Nuetral</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s5['id'] ?>]" value="-1">Disagree</label></div>
                                <div class="radio"><label><input type="radio" name="question[<?= $s5['id'] ?>]" value="-2">Strongly Disagree</label></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                    <input type="button" name="previous" class="previous btn btn-default" value="Previous" />
                    <input type="submit" name="submit" class="submit btn btn-success" value="Submit" />
                </fieldset>
            <?= form_close()?>
        </div>
    </div>
</div>
