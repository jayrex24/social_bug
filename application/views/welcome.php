<? $quoteArray = array('“Live for yourself.”',
	'“Brevity is beautiful.”',
	'“Work hard. Dream big.”',
	'“Life is short. Live passionately.”',
	'“Whatever you are, be a good one.”',
	'“Everything happens for a reason.”',
	'“Be the change you wish to see in the world.”',
	'“Don’t regret the past, just learn from it.”'
);
?>

<header>
	<div class="container">
		<div class="intro-text">
			<div class="col-lg-8 col-lg-offset-2">
				<? if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true):?>
					<div class="intro-lead-in">Welcome <?= $_SESSION['username']; ?>!</div>
					<div class="intro-heading"><?= $quoteArray[array_rand($quoteArray)];  ?></div>
					<a href="<?= base_url('user_profile/user_home'); ?>" class="page-scroll btn btn-info">Get Started!</a>
				<? else: ?>
					<div class="intro-lead-in">Welcome To Social Bug!</div>
					<div class="intro-heading"><?= $quoteArray[array_rand($quoteArray)]; ?></div>
					<a href="<?= base_url('/user/register'); ?>" class="page-scroll btn btn-info">Sign up today!</a>
				<? endif; ?>
			</div>
		</div><!-- /row -->
	</div> <!-- /container -->
</header><!-- /headerwrap -->
