<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $active_menu;?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">

	<!-- Fonts / Font Awesome -->
	<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

	<!-- Favicon -->
	<link rel="icon" href="/assets/images/Logo.png?" type="image/x-icon">

	<!-- JQuery -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/flick/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<!-- Bootstrap -->
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/united/bootstrap.min.css" rel="stylesheet" integrity="sha384-pVJelSCJ58Og1XDc2E95RVYHZDPb9AVyXsI8NoVpB2xmtxoZKJePbMfE4mlXw7BJ" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">

	<!-- Custom styles for this template -->
	<link href="/assets/css/style.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation_bar" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?= base_url() ?>">Social Bug <img src="/assets/images/Logo.png" class="pull-right" alt="butterfly" height='30' width='30'></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="navbar-collapse collapse navbar-right" id="navigation_bar">
			<ul class="nav navbar-nav">
			<? if (isset($_SESSION['logged_in']) === true) : ?>
				<li class="dropdown
				<?= isset($active_menu)
					&& $active_menu == 'User Matches'
					|| $active_menu == 'Survey'
					|| $active_menu == 'Socialize'
					|| $active_menu == 'User Opposites' ? 'active' : '' ?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Socialize <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li <? if(!empty($active_menu) && $active_menu== 'Socialize') : ?>class='active'<?endif;?>><a href="<?= base_url('socialize/index') ?>">Socialize</a></li>
						<li role="separator" class="divider"></li>
						<li <? if(!empty($active_menu) && $active_menu== 'User Matches') : ?>class='active'<?endif;?>><a href="<?= base_url('find/user_matches') ?>">Matches</a></li>
						<li <? if(!empty($active_menu) && $active_menu== 'User Opposites') : ?>class='active'<?endif;?>><a href="<?= base_url('find/user_opposites') ?>">Opposites</a></li>
						<li role="separator" class="divider"></li>
						<li <? if(!empty($active_menu) && $active_menu== 'Survey') : ?>class='active'<?endif;?>><a href="<?= base_url('user_survey/take_survey') ?>">Personality Test</a></li>
					</ul>
				</li>
				<li class="dropdown
					<?= isset($active_menu)
						&& $active_menu == $_SESSION['username']."'s page"
						|| $active_menu=='User Profile'
						|| $active_menu=='Update Password'
						|| $active_menu=='About'
						|| $active_menu=='View Gallery'
						|| $active_menu=='Upload Photos'
						|| $active_menu=='Upload Videos' ? 'active' : ''
					?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $_SESSION['username']; ?> <span class="caret"></span></a>
					<ul class="dropdown-menu">
                        <li <? if(!empty($active_menu) && $active_menu== $_SESSION['username']."'s page"): ?>class='active'<?endif;?>><a href="<?= base_url('user_page/user/'.$_SESSION['username']) ?>">View Profile</a></li>
                        <li role="separator" class="divider"></li>
						<li <? if(!empty($active_menu) && $active_menu=='User Profile'): ?>class='active'<?endif;?>><a href="<?= base_url('user_profile/user_home') ?>">Update Profile</a></li>
						<li <? if(!empty($active_menu) && $active_menu=='Update Password'): ?>class='active'<?endif;?>><a href="<?= base_url('user_profile/change_password') ?>">Update Password</a></li>
                        <li <? if(!empty($active_menu) && $active_menu=='About'): ?>class='active'<?endif;?>><a href="<?= base_url('user_profile/about') ?>">Update About</a></li>
						<li role="separator" class="divider"></li>
						<li <? if(!empty($active_menu) && $active_menu=='View Gallery'): ?>class='active'<?endif;?>><a href="<?= base_url('user_media/view_gallery') ?>">View Gallery</a></li>
						<li <? if(!empty($active_menu) && $active_menu=='Upload Photos'): ?>class='active'<?endif;?>><a href="<?= base_url('user_media/upload_photos') ?>">Upload Photos</a></li>
						<li role="separator" class="divider"></li>
						<li <? if(!empty($active_menu) && $active_menu== 'Inbox'): ?>class='active'<?endif;?>><a href="<?= base_url('pm/inbox/') ?>">View Messages</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="<?= base_url('logout') ?>">Log out</a></li>
					</ul>
				</li>
			<? else : ?>
				<li <? if(!empty($active_menu) && $active_menu=='Socialize'): ?>class='active'<?endif;?>><a href="<?= base_url('socialize') ?>">Socialize</a></li>
				<li <? if(!empty($active_menu) && $active_menu=='Register'): ?>class='active'<?endif;?>><a href="<?= base_url('user/register') ?>">Sign Up</a></li>
				<li <? if(!empty($active_menu) && $active_menu=='Login'): ?>class='active'<?endif;?>><a href="<?= base_url('user/login') ?>">Log In</a></li>
			<? endif; ?>
			</ul>
		</div>
	</div>
</nav>

