            </div>
        </div>
    </div>
</div>
<footer id="mainfooter">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-4 mt" align="center">
                <ul class="list-inline social-buttons">
                    <li><a href="http://www.facebook.com/jayson.malabanan" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/jayson-malabanan-b8985645" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="https://github.com/jayrex24" target="_blank"><i class="fa fa-github"></i></a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h2>About Us</h2>
                <small class="text-justify">
                    Students at UMBC seem to have trouble finding people similar to them, but there may bee a helpful tool that will help students accomplish this.
                    Social Bug will be a one-stop location for meeting people on campus that you can potentially spark a relationship with.
                    This website will allow students to connect to the site with their customizable profile, take a personality test, browse through people that they
                    matched closely, and send messages to other users on the site.
                </small>
                <br />
                <br />
            </div>
        </div>
    </div>

</footer>
<footer id="copyrightfooter">
        <span class="copyright">&copy;<?= date("Y")?> socialbug.<a href="http://www.jaysonmalabanan.com" target="_blank">jaysonmalabanan.com</a></span><br/>
        <small class="text-muted">Page Load Time: <?= $this->benchmark->elapsed_time()?></small>
</footer>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>

<!-- Custom JS -->
<script src="/assets/js/label.js"></script>
<script src="/assets/js/custom.js"></script>
</body>
</html>