<?
	$this->load->helper('states');
	function add_quotes($str) {
	    return sprintf("'%s'", ucwords(strtolower($str)));
	}
	$states = implode(',', array_map('add_quotes', getStates()));
?>

<script type="text/javascript">
	$(document).ready(function (){
		var availableTags = [<?= $states ?>];
		$("#state").autocomplete({
			maxShowItems: 10,
			source: availableTags
		});
	});

	function datePicker() {
		$("#birth_date").datepicker(
			{
				minDate: new Date(1900,1-1,1), maxDate: '-18Y',
				dateFormat: 'MM d, yy',
				changeMonth: true,
				changeYear: true,
				yearRange: '-110:-18'
			}
		);
	}
</script>

<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
			<?= form_open() ?>
				<div class="form-group row">
				<label for="first_name" class="col-sm-3 form-control-label">First Name</label>
					<div class="col-sm-6">
					  <input type="text" class="form-control" id="first_name" name="first_name" value="<?= !empty($first_name) ? $first_name : '' ?>" placeholder="Enter First Name">
					</div>
				</div>
				<div class="form-group row">
				<label for="last_name" class="col-sm-3 form-control-label">Last Name</label>
					<div class="col-sm-6">
					  <input type="text" class="form-control" id="last_name" name="last_name" value="<?= !empty($last_name) ? $last_name : '' ?>" placeholder="Enter Last Name">
					</div>
				</div>
				<div class="form-group row">
					<label for="birth_date" class="col-sm-3 form-control-label">Birth Date</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="birth_date" name="birth_date" value="<?= !empty($birth_date) ? $birth_date : '' ?>" placeholder="Enter Birth Date" onmouseenter="datePicker()">
					</div>
				</div>
				<div class="form-group row">
					<label for="address" class="col-sm-3 form-control-label">Street Address</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="address" name="address" value="<?= !empty($address) ? $address : '' ?>" placeholder="Enter Address">
					</div>
				</div>
				<div class="form-group row">
					<label for="city" class="col-sm-3 form-control-label">City</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="city" name="city" value="<?= !empty($city) ? $city : '' ?>" placeholder="Enter City">
					</div>
				</div>
				<div class="form-group row">
					<label for="state" class="col-sm-3 form-control-label">State</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="state" name="state" value="<?= !empty($state) ? $state : '' ?>" placeholder="Enter State">
					</div>
				</div>
				<div class="form-group row">
					<label for="zip" class="col-sm-3 form-control-label">Zip Code</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" id="zip" name="zip" value="<?= !empty($zip) ? $zip : '' ?>" placeholder="Enter Zip Code">
					</div>
				</div>
				<hr>
				<input type="submit" class="btn btn-primary" name="infochange" id="infochange" value="Save">
			<?= form_close() ?>
		</div>
	</div>
</div>