<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <?= form_open() ?>
                <div class="form-group row">
                    <label for="current_password" class="col-sm-3 form-control-label">Current Password</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Enter Current Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-3 form-control-label">New Password</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password_confirm" class="col-sm-3 form-control-label">New Password (Confirm)</label>
                    <div class="col-sm-6">
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm new password (Case Sensitive)">
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <input type="submit" class="btn btn-primary" name="passwordchange" value="Save">
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>