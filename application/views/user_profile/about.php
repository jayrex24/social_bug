<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <?=  form_open() ?>
                <div class="form-group row">
                    <label for="interests" class="col-sm-2 form-control-label">Interest</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="5" id="interests" name="interests" placeholder="Share Your Interests"><?= isset($interests) ? $interests : '' ?></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="favorite_movies" class="col-sm-2 form-control-label">Favorite Movies</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="5" id="favorite_movies" name="favorite_movies" placeholder="Share Your Favorite Movies"><?= isset($favorite_movies) ? $favorite_movies : '' ?></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="favorite_books" class="col-sm-2 form-control-label">Favorite Books</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" rows="5" id="favorite_books" name="favorite_books" placeholder="Share Your Favorite Books" ><?= isset($favorite_books) ? $favorite_books : '' ?></textarea>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <input type="submit" class="btn btn-primary" name="aboutchange" value="Save">
                    </div>
                </div>
            <?=  form_close() ?>
        </div>
    </div>
</div>
