<div class="panel-body">
    <div class="container spacer">
        <h2 class="centered">Matches</h2>
        <p class="text-muted centered"><i>Time to make friends</i></p>
        <div class="row">
            <div class="col-md-12">
                <? if(empty($matches)): ?>
                    <h4 class="media-heading">The right person will come</h4>
                <? else:
                    foreach($matches as $k => $user):
                        $profile_pic = $this->user_photos_model->getProfilePic($user['id']);
                        if(!empty($profile_pic)){
                            $profile_pic_path = $profile_pic[0]['location'];
                        }else{
                            $profile_pic_path = "/assets/images/default.png";
                        }
                        ?>
                        <div class="col-md-3">
                            <div class="well well-sm">
                                <div class="media" style="min-height: 23em;">
                                    <a class="thumbnail centered" href="<?= base_url('user_page/user/'.$user['username'])?>">
                                        <img class="img-rounded" src="<?= $profile_pic_path ?>" style="max-height: 10em; min-height: 10em;">
                                    </a>
                                    <div class="media-body centered">
                                        <h4 class="media-heading centered"><?= $user['username']; ?></h4>
                                        <? if(isset($_SESSION['logged_in'])): ?>
                                            <?  if($user['id'] == $_SESSION['user_id']){ ?>
                                                <h6>Your Profile</h6>
                                            <? }elseif(in_array($user['id'], $following)){ ?>
                                                <?= form_open(); ?>
                                                <input type="hidden" name="unfollow_id" id="unfollow_id" value="<?= $user['id'] ?>">
                                                <button type="submit" class="btn btn-xs btn-danger" name="unfollowUser" id="unfollowUser"><i class="fa fa-minus-circle" aria-hidden="true"></i> Unfollow</button>
                                                <?= form_close(); ?>
                                            <? } else { ?>
                                                <?= form_open(); ?>
                                                <input type="hidden" name="search_bar" id="search_bar"">
                                                <input type="hidden" name="follow_id" id="follow_id" value="<?= $user['id'] ?>">
                                                <button type="submit" class="btn btn-xs btn-primary" name="followUser" id="followUser"><i class="fa fa-plus-circle" aria-hidden="true"></i> Follow</button>
                                                <?= form_close(); ?>
                                            <? } ?>
                                        <? endif; ?>
                                        <?
                                        $follower_count = $this->followers_model->getFollowers($user['id']);
                                        $following_count = $this->followers_model->getFollowing($user['id']);
                                        ?>
                                        <p>
                                            <span class="label label-warning"><?= count($follower_count) > 1 ? count($follower_count)." followers" : count($follower_count)." follower" ?></span>
                                            <span class="label label-info"><?= count($following_count) ?> Following</span>
                                        </p>
                                        <p>
                                            <a href="<?= base_url('user_page/user/'.$user['username'])?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-eye-open"></span> Visit Page</a>
                                            <? if(isset($_SESSION['logged_in'])):
                                                if($_SESSION['username'] != $user['username']): ?>
                                                    <a href="<?= base_url('pm/compose/'.$user['username'])?>" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-comment"></span> Message</a>
                                                <? endif; ?>
                                            <? endif; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach;
                endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" align="center">
                <?= $pagination; ?>
            </div>
        </div>
    </div>
</div>