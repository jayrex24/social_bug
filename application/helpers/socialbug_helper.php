<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pp'))
{
    function pp($myArray) {
    	echo "<pre>";
		print_r($myArray);
		echo "</pre>";
    }
}

if ( ! function_exists('pluralize'))
{
    function pluralize($count, $singular, $plural = false){
        if (!$plural) $plural = $singular . 's';

        return ($count == 1 || $count == 0 ? $singular : $plural) ;
    }
}

if ( ! function_exists('curl_get_contents'))
{
    function curl_get_contents($url)
    {
        $ch = curl_init($url);

        if ($ch == FALSE) {
            return array('error' =>"failed");
        }else{
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($ch, CURLOPT_VERBOSE, 0);

            $result = curl_exec($ch);
            return $result;
        }
    }
}

if ( ! function_exists('getFollower'))
{
    function getFollower($user_id, $follow_id, $unfollow_id){
        $followers_model = new Followers_Model();

        $following = null;
        if(isset($_SESSION['logged_in'])){
            if(!empty($follow_id)){
                $followers_model->addFollowing($user_id,$follow_id);
            }
            if(!empty($unfollow_id)){
                $followers_model->removeFollowing($user_id,$unfollow_id);
            }
            $following = $followers_model->getFollowing($_SESSION['user_id']);
        }
        return $following;
    }
}

if ( ! function_exists('paginationSettings'))
{
    function paginationSettings($base_url, $total_rows, $per_page, $uri_segment){

        // pagination settings
        $config = array();
        $config['base_url']    = $base_url;
        $config['total_rows']  = $total_rows;
        $config['per_page']    = $per_page;
        $config["uri_segment"] = $uri_segment;
        $config["num_links"]   = 2;

        // integrate bootstrap pagination
        $config['full_tag_open']   = '<ul class="pagination">';
        $config['full_tag_close']  = '</ul>';
        $config['first_link']      = false;
        $config['last_link']       = false;
        $config['first_tag_open']  = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link']       = '«';
        $config['prev_tag_open']   = '<li class="prev">';
        $config['prev_tag_close']  = '</li>';
        $config['next_link']       = '»';
        $config['next_tag_open']   = '<li>';
        $config['next_tag_close']  = '</li>';
        $config['last_tag_open']   = '<li>';
        $config['last_tag_close']  = '</li>';
        $config['cur_tag_open']    = '<li class="active"><a href="#">';
        $config['cur_tag_close']   = '</a></li>';
        $config['num_tag_open']    = '<li>';
        $config['num_tag_close']   = '</li>';

        return $config;
    }
}

