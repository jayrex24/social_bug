<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 300);

class Random_User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('socialbug'));
        $this->load->model(array('random_user_model', 'user_model'));
        $this->load->helper('url');
    }

    public function generate()
    {

        $generator_on = true;
        $get_amount = 50;

        if ($generator_on == true) {

            $url = 'https://randomuser.me/api/?results=' . $get_amount;

            $json = curl_get_contents($url);
            $user_array = json_decode($json, true);

            $date = date('Y-m-d H:i:s');
            $bugtypes = $this->random_user_model->getAllBugType();
            $bugtypes[] = ""; // add users without bugtype

            foreach ($user_array['results'] as $user) {

                $enc_pass = $this->user_model->hash_password($user['login']['password']);
                $user_bugtype = $bugtypes[array_rand($bugtypes)];

                // replace character so it doesn't escape or cause errors
                $username = str_replace("'", "`", $user['login']['username']);
                $first_name = str_replace("'", "`", $user['name']['first']);
                $last_name = str_replace("'", "`", $user['name']['last']);
                $birth_date = str_replace("'", "`", $user['dob']);
                $address = str_replace("'", "`", $user['location']['street']);
                $state = str_replace("'", "`", $user['location']['state']);
                $zip = str_replace("'", "`", $user['location']['postcode']);
                $city = str_replace("'", "`", $user['location']['city']);
                $email = str_replace("'", "`", $user['email']);


                $user_info = array(
                    'username' => $username,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'birth_date' => date('F d, Y', strtotime($birth_date)),
                    'address' => $address,
                    'state' => $state,
                    'zip' => $zip,
                    'city' => $city,
                    'email' => $email,
                    'password' => $enc_pass,
                    'created_at' => $date,
                    'interests' => '',
                    'favorite_movies' => '',
                    'favorite_books' => '',
                    'bugtype' => $user_bugtype,
                );

                $userId = $this->random_user_model->insertGeneratedUser($user_info);

                $image_name = substr($user['picture']['large'], strrpos($user['picture']['large'], '/') + 1);

                $user_images = array(
                    'user_id' => $userId,
                    'location' => $user['picture']['large'],
                    'image_name' => $image_name,
                    'description' => '',
                    'image_type' => 'image/jpeg',
                    'set_profile' => 1,
                );

                $this->random_user_model->insertProfilePhoto($user_images);
            }
        }
    }
}