<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_settings_model','followers_model'));
        $this->load->model(array('user_auth_model','user_sidebar_model'));
        $this->load->helper(array('form', 'url','socialbug','states'));
        $this->load->library(array('session','form_validation','email','pagination'));
    }

    public function user_home() {
        $this->user_auth_model->is_logged_out();
        $page = 'User Profile';
        $this->success = null;
        $this->errors  = null;

        $id         = $this->session->all_userdata();
        $user_info  = (array) $this->user_model->get_user($id['user_id']);

        if($this->input->post('infochange')){

            if($this->input->post('first_name') !== $user_info['first_name']){
                if($this->input->post('first_name')){
                    $this->success[] = 'First name updated to <strong>' .$this->input->post('first_name').'</strong>';
                }else{
                    $this->success[] = 'Deleted first name';
                }
                $binds['first_name'] = $this->input->post('first_name');
            }

            if($this->input->post('last_name') !== $user_info['last_name']){
                if($this->input->post('last_name')){
                    $this->success[] = 'Last name updated to <strong>' . $this->input->post('last_name') . '</strong>';
                }else {
                    $this->success[] = 'Deleted last name';
                }
                $binds['last_name'] = $this->input->post('last_name');
            }

            if($this->input->post('birth_date') !== $user_info['birth_date']){
                if($this->input->post('last_name')){
                    $this->success[] = 'Birth date updated to <strong>' .$this->input->post('birth_date').'</strong>';
                }else {
                    $this->success[] = 'Deleted birth date';
                }
                $binds['birth_date'] = $this->input->post('birth_date');
            }

            if($this->input->post('address') !== $user_info['address']){
                if($this->input->post('address')){
                    $this->success[] = 'Address updated to <strong>' .$this->input->post('address').'</strong>';
                }else {
                    $this->success[] = 'Deleted address';
                }

                $binds['address'] = $this->input->post('address');
            }

            if($this->input->post('city') !== $user_info['city']){
                if($this->input->post('address')){
                    $this->success[] = 'City updated to <strong>' .$this->input->post('city').'</strong>';
                }else {
                    $this->success[] = 'Deleted city';
                }
                $binds['city'] = $this->input->post('city');
            }

            if($this->input->post('state') !== $user_info['state']){
                if($this->input->post('state')){
                    $this->success[] = 'State updated to <strong>' .$this->input->post('state').'</strong>';
                }else {
                    $this->success[] = 'Deleted state';
                }
                $binds['state'] = $this->input->post('state');
            }

            if($this->input->post('zip') !== $user_info['zip']){
                if($this->input->post('zip')){
                    $this->success['zip'] = 'Zip updated to <strong>' .$this->input->post('zip').'</strong>';
                }else {
                    $this->success[] = 'Deleted zip';
                }
                $binds['zip'] = $this->input->post('zip');
            }

            if(!empty($binds)){
                $this->user_settings_model->update_information($binds, $id);
                $user_info = (array) $this->user_model->get_user($id['user_id']);
            }else{
                $this->errors[] = "Must change/fill at least one field.";
            }
        }

        $pagevars = array(
            'active_menu' => $page,
            'first_name'  => $user_info['first_name'],
            'last_name'   => $user_info['last_name'],
            'birth_date'  => $user_info['birth_date'],
            'address'     => $user_info['address'],
            'city'   	  => $user_info['city'],
            'state'   	  => $user_info['state'],
            'zip'		  => $user_info['zip'],
            'success'     => $this->success,
            'errors'      => $this->errors,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_profile/user_home');
        $this->load->view('footer');
    }

    public function change_password() {
        $this->user_auth_model->is_logged_out();
        $page = 'Update Password';
        $this->success = null;
        $this->errors  = null;

        $user_data = $this->session->all_userdata();

        $this->form_validation->set_rules('current_password', 'Current password', 'trim|min_length[6]|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|min_length[6]|matches[password_confirm]|required');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'trim|required');

        if ($this->form_validation->run() == true) {
            if ($this->user_model->resolve_user_login($user_data['username'], $this->input->post('current_password'))) {
                $new_password = $this->user_model->hash_password($this->input->post('password'));
                $this->user_settings_model->update_password($new_password, $user_data['user_id']);
                $this->success['pass_updated'] = 'Password Updated';
            } else {
                $this->errors['old_password_check'] = 'Current password does not match!';
            }
        }

        $pagevars = array(
            'active_menu' => $page,
            'success'     => $this->success,
            'errors'      => $this->errors,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_profile/change_password');
        $this->load->view('footer');
    }

    public function about(){
        $this->user_auth_model->is_logged_out();
        $page = 'About';
        $this->success = null;
        $this->errors  = null;

        $id         = $this->session->all_userdata();
        $user_info  = (array) $this->user_model->get_user($id['user_id']);

        if($this->input->post('aboutchange')){

            if($this->input->post('interests') !== $user_info['interests']){
                if($this->input->post('interests')){
                    $this->success[] = 'Interests updated';
                }else {
                    $this->success[] = 'Deleted interests';
                }
                $binds['interests'] = $this->input->post('interests');
            }
            if($this->input->post('favorite_movies') !== $user_info['favorite_movies']){
                if($this->input->post('interests')){
                    $this->success[] = 'Favorite movies updated';
                }else {
                    $this->success[] = 'Deleted favorite movies';
                }
                $binds['favorite_movies'] = $this->input->post('favorite_movies');
            }
            if($this->input->post('favorite_books') !== $user_info['favorite_books']){
                if($this->input->post('interests')){
                    $this->success[] = 'Favorite books updated';
                }else {
                    $this->success[] = 'Deleted favorite books';
                }
                $binds['favorite_books'] = $this->input->post('favorite_books');
            }

            if(!empty($binds)){
                $this->user_settings_model->update_about($binds, $id);
                $user_info = (array) $this->user_model->get_user($id['user_id']);
            }else{
                $this->errors[] = "Must change/fill at least one field.";
            }

        }

        $pagevars = array(
            'active_menu'     => $page,
            'interests'       => $user_info['interests'],
            'favorite_movies' => $user_info['favorite_movies'],
            'favorite_books'  => $user_info['favorite_books'],
            'success'         => $this->success,
            'errors'          => $this->errors,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_profile/about');
        $this->load->view('footer');
    }
}
