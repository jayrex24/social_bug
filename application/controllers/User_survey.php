<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_survey extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_photos_model','followers_model', 'survey_model'));
        $this->load->model(array('user_auth_model','user_sidebar_model'));
        $this->load->helper(array('form', 'url','socialbug'));
        $this->load->library(array('session','form_validation','pagination'));
    }

    public function take_survey() {
        $this->user_auth_model->is_logged_out();
        $page_name = 'Survey';
        $this->success = null;
        $this->errors = null;
        $bugtype = null;

        $id         = $this->session->all_userdata();
        $user_info  = (array) $this->user_model->get_user($id['user_id']);

        $survey_questions = $this->survey_model->getQuestions();
        $survey = array_chunk($survey_questions,10);

        if($this->input->post('submit')){
            $result = $_POST['question'];

            $mind    = $result[1]  - $result[4] - $result[6]  + $result[13] - $result[17] - $result[23] - $result[28] + $result[31] + $result[42] + $result[43] - $result[45] + $result[47] + $result[50] - $result[52] + $result[53] - $result[54] - $result[55] + $result[60];
            $energy  = $result[20] - $result[7] - $result[12] - $result[14] + $result[24] + $result[25] + $result[30] - $result[32] + $result[35] - $result[37] + $result[51] + $result[55] - $result[56] + $result[58];
            $nature  = $result[2]  - $result[9] - $result[11] + $result[14] + $result[15] - $result[18] - $result[20] - $result[22] - $result[26] + $result[28] - $result[29] - $result[33] + $result[36] - $result[40] + $result[43] - $result[48] + $result[49] - $result[53] + $result[56] - $result[57] + $result[59];
            $tactics = $result[2]  + $result[3] - $result[8]  + $result[10] + $result[16] - $result[19] - $result[21] + $result[23] + $result[32] - $result[35] - $result[38] - $result[41] - $result[44] + $result[45] - $result[46] + $result[50] + $result[56];

            $mind    > 0 ? $type1 = "E" : $type1 = "I";
            $energy  > 0 ? $type2 = "S" : $type2 = "N";
            $nature  > 0 ? $type3 = "T" : $type3 = "F";
            $tactics > 0 ? $type4 = "J" : $type4 = "P";

            $type = $type1.$type2.$type3.$type4;

            switch ($type){
                case 'INTJ':
                    $bugtype = "Fire Ant";
                    break;
                case 'INTP':
                    $bugtype = "Cockroach";
                    break;
                case 'ENTJ':
                    $bugtype = "Bumble Bee";
                    break;
                case 'ENTP':
                    $bugtype = "Grasshopper";
                    break;
                case 'INFJ':
                    $bugtype = "Hercules Beetle";
                    break;
                case 'INFP':
                    $bugtype = "Daddy Longlegs";
                    break;
                case 'ENFJ':
                    $bugtype = "Dragonfly";
                    break;
                case 'ENFP':
                    $bugtype = "Cricket";
                    break;
                case 'ISTJ':
                    $bugtype = "Walkingstick";
                    break;
                case 'ISFJ':
                    $bugtype = "Wasp";
                    break;
                case 'ESTJ':
                    $bugtype = "Lady Bug";
                    break;
                case 'ESFJ':
                    $bugtype = "Praying Mantis";
                    break;
                case 'ISTP':
                    $bugtype = "Black Widow";
                    break;
                case 'ISFP':
                    $bugtype = "Monarch Butterfly";
                    break;
                case 'ESTP':
                    $bugtype = "Luna Moth";
                    break;
                case 'ESFP':
                    $bugtype = "Fly";
                    break;
                default:
                    break;
            }

            $bugtype_desc = $this->survey_model->getBugtypeByName($bugtype);

            $this->success[] = sprintf("You've completed the personality test! Your bug type is <strong>%s!</strong><br/> &ldquo;%s&rdquo;", $bugtype, $bugtype_desc[0]['description']);
            $this->survey_model->updateBugtype($_SESSION['user_id'], $bugtype);
        }

        if(!empty($user_info['bugtype'])){
            $greeting = "Not convinced? Take the personality test again";
        }else{
            $greeting = "Start Your Personality Test";
        }

        $pagevars = array(
            'active_menu' => $page_name,
            'success'     => $this->success,
            'errors'      => $this->errors,
            'survey'      => $survey,
            'greeting'    => $greeting,
            'bugtype'     => isset($bugtype) ? $bugtype : $user_info['bugtype'],
        );

        $this->load->view('header',$pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_survey/user_survey');
        $this->load->view('footer');
    }
}
