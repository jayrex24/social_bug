<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_settings_model','sendmail_model','user_photos_model','user_auth_model'));
        $this->load->helper(array('form', 'url','socialbug','states'));
        $this->load->library(array('session','form_validation','email'));
    }
	
	public function index() {
		$page = 'Home';

		$pagevars = array(
			'active_menu' => $page,
		);
		
		$this->load->view('header', $pagevars);
		$this->load->view('welcome');
		$this->load->view('footer');
	}

	public function register() {
        $this->user_auth_model->is_logged_in();
		$page = 'Register';

		// create the data object
		$data = new stdClass();

		$pagevars = array(
			'active_menu' => $page,
		);
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[3]|max_length[15]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose a different one.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]', array('is_unique' => 'This email already exists. Please choose a different one.'));
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
		
		if ($this->form_validation->run() == true) {
			// set variables from the form
			$username = $this->input->post('username');
			$email    = $this->input->post('email');
			$password = $this->input->post('password');
			
			if ($this->user_model->create_user($username, $email, $password)) {

				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);
				
				// set session user datas
				$_SESSION['user_id']   = (int)$user->id;
				$_SESSION['username']  = (string)$user->username;
				$_SESSION['logged_in'] = (bool)true;

                $trackinInfo = array(
                    'user_id'    => (int) $user->id,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                );

                $this->user_model->trackingInfo($trackinInfo);

                $this->sendmail_model->sendEmail("New User", $email, (string)$user->username);

                redirect(base_url('user_profile/user_home')."?newuser=".$_SESSION['username']);
			} else {
				// user creation failed, this should never happen
                $this->session->set_flashdata('flash_message', 'There was a problem creating your new account. Please try again.');
			}
		}

        $this->load->view('header', $pagevars);
        $this->load->view('user/register/register', $data);
        $this->load->view('footer');
	}

	public function login() {
        $this->user_auth_model->is_logged_in();
		$page = 'Login';
		
		// create the data object
		$data = new stdClass();

		$pagevars = array(
			'active_menu' => $page,
		);
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true) {
			
			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if ($this->user_model->resolve_user_login($username, $password)) {
				
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);
				
				// set session user datas
				$_SESSION['user_id']   = (int)$user->id;
				$_SESSION['username']  = (string)$user->username;
				$_SESSION['logged_in'] = (bool)true;

                $trackinInfo = array(
                    'user_id'    => (int)$user->id,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                );

                $this->user_model->trackingInfo($trackinInfo);

                redirect(base_url('user_profile/user_home'));
			} else {
				// login failed
				$data->error = 'Wrong username or password.';
			}
		}

        $this->load->view('header', $pagevars);
        $this->load->view('user/login/login', $data);
        $this->load->view('footer');
	}

	public function forgot_password() {
        $this->user_auth_model->is_logged_in();
		$page = 'Forgot Password';

        // create the data object
        $data = new stdClass();

		$pagevars = array(
			'active_menu' => $page,
		);

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if($this->form_validation->run() == true) {

            $email = $this->input->post('email');
            $clean = $this->security->xss_clean($email);
            $userInfo = $this->user_model->getUserInfoByEmail($clean);

            if(!$userInfo){
                $data->error = 'We can\'t find your email address in our system.';

            }else{
                $data->success = 'We\'ve sent you an email regarding your request.';

                //remove existing token for user if any
                $this->user_model->removeToken($userInfo->id);

                //build token
                $token = $this->user_model->insertToken($userInfo->id);
                $qstring = $this->base64url_encode($token);
                $link = base_url().'user/reset_password/'. $qstring;

                $message  = sprintf("Hello %s, \r\n\r\n", $userInfo->username);
                $message .= "A password reset has been requested for this email account. \r\n";
                $message .= "Click Here to reset your password: \r\n". $link . "\r\n\r\n";
                $message .= "Thanks, \r\nSocial Bug Admin";

                $this->sendmail_model->sendEmail("Password Reset", $userInfo->email, $userInfo->username, $message);
            }
        }

		$this->load->view('header', $pagevars);
		$this->load->view('user/login/forgot_password', $data);
		$this->load->view('footer');
	}

    public function reset_password() {
        $this->user_auth_model->is_logged_in();
        $page = 'Reset Password';

        $data = new stdClass();

        $pagevars = array(
            'active_menu' => $page,
        );

		$token = $this->base64url_decode($this->uri->segment(3));
		$cleanToken = $this->security->xss_clean($token);
		$user_info = $this->user_model->isTokenValid($cleanToken); //either false or array();

		if(!$user_info){
            $this->session->set_flashdata('error', 'Reset password link not valid or expired.');
            redirect(base_url('user/login'));
		}

		$data = array(
			'username' => $user_info->username,
			'email'    => $user_info->email,
			'token'    => base64_encode($token)
		);

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

        if ($this->form_validation->run() == true) {

			$post = $this->input->post(null, true);
			$cleanPost = $this->security->xss_clean($post);
			$hashed_pass = $this->user_model->hash_password($cleanPost['password']);
			unset($cleanPost['passconf']);
			if(!$this->user_settings_model->update_password($hashed_pass, $user_info->id)){
				$this->session->set_flashdata('error', 'There was a problem updating your password');
			}else{
				$this->session->set_flashdata('success', 'Your password has been updated. You may now login');
                $this->user_model->removeToken($user_info->id);
			}
			redirect(base_url('user/login'));
		}

        $this->load->view('header', $pagevars);
        $this->load->view('user/login/reset_password', $data);
        $this->load->view('footer');
    }

	public function logout() {
		// create the data object
		$data = new stdClass();
		
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			redirect(base_url('user/login'));
		} else {
            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            redirect(base_url());
        }
	}

    public function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    public function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
}
