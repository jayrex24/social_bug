<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Pm extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model', 'user_photos_model', 'followers_model', 'survey_model','user_auth_model','user_sidebar_model','mahana_model'));
        $this->load->helper(array('form', 'url', 'socialbug'));
        $this->load->library(array('session', 'form_validation', 'pagination'));
        $this->load->library('encrypt');
    }

    public function compose(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'Compose';
        $this->success = null;
        $this->errors = null;

        $username = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $recipient = (array) $this->user_model->get_user_info_from_username($username);
        $profile_photo = $this->user_photos_model->getProfilePic($recipient['id']);

        if($this->input->post('submit')){
            $message_to_send = $this->mahana_model->send_new_message($_SESSION['user_id'], $recipient['id'], $this->input->post('subject'), $this->input->post('message'), 2);
            if(!empty($message_to_send)){
                $this->success[] = "Message Sent";
            }
        }

        $pagevars = array(
            'active_menu' => $page_title,
            'success'     => $this->success,
            'errors'      => $this->errors,
            'message_to'  => $username,
            'profile'     => !empty($profile_photo) ? '/'.$profile_photo[0]['location'] : "/assets/images/default.png",
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_messages/compose');
        $this->load->view('footer');
    }

    public function inbox(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'Inbox';
        $this->success = null;
        $this->errors = null;

        $message_count = $this->mahana_model->get_msg_count($_SESSION['user_id']);
        $thread = $this->mahana_model->get_all_threads($_SESSION['user_id']);

        $pagevars = array(
            'active_menu'   => $page_title,
            'success'       => $this->success,
            'errors'        => $this->errors,
            'message_count' => isset($message_count) ? $message_count : 0,
            'thread'        => isset($thread) ? $thread : 0,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_messages/inbox');
        $this->load->view('footer');
    }

    public function sent(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'Sent';
        $this->success = null;
        $this->errors = null;

        $message_count = $this->mahana_model->get_msg_count($_SESSION['user_id']);
        $thread = $this->mahana_model->get_all_threads($_SESSION['user_id']);

        $pagevars = array(
            'active_menu'   => $page_title,
            'success'       => $this->success,
            'errors'        => $this->errors,
            'message_count' => isset($message_count) ? $message_count : 0,
            'thread'        => isset($thread) ? $thread : 0,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_messages/sent');
        $this->load->view('footer');
    }

    public function read(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'Message';
        $this->success = null;
        $this->errors = null;

        $enc_key = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $msg_id = str_replace(array('-', '_', '~'), array('+', '/', '='), $enc_key);
        $msg_id = $this->encrypt->decode($msg_id);

        $this->mahana_model->update_message_status($msg_id, $_SESSION['user_id'], 1);
        $full_thread = $this->mahana_model->get_full_thread($msg_id, $_SESSION['user_id'], $full_thread = FALSE, $order_by = 'desc');

        if($full_thread == 0){
            redirect('pm/inbox');
        }

        if($this->input->post('submit')){
            unset($full_thread);
            $this->mahana_model->reply_to_message($msg_id, $_SESSION['user_id'], $this->input->post('reply'), 2);
            $full_thread = $this->mahana_model->get_full_thread($msg_id, $_SESSION['user_id'], $full_thread = FALSE, $order_by = 'desc');
        }

        $pagevars = array(
            'active_menu'   => isset($full_thread[0]['subject']) ? $full_thread[0]['subject'] : $page_title,
            'success'       => $this->success,
            'errors'        => $this->errors,
            'message'       => $full_thread,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_messages/read');
        $this->load->view('footer');
    }
}