<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_Page extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_photos_model','followers_model','survey_model'));
        $this->load->helper(array('form', 'url','socialbug'));
        $this->load->library(array('session','form_validation','pagination'));
    }

    public function user() {
        $profile_pic_path = null;

        $username = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $user_info = $this->user_model->get_user_info_from_username($username);

        $profile_pic = $this->user_photos_model->getProfilePic($user_info->id);
        if(!empty($profile_pic)){
            $profile_pic_path = $profile_pic[0]['location'];
        }else{
            $profile_pic_path = "/assets/images/default.png";
        }

        $page_title = $username.'\'s page';

        $following = null;
        if(isset($_SESSION['logged_in'])){
            if(isset($_POST['followUser'])){
                $this->followers_model->addFollowing($_SESSION['user_id'],$this->input->post('follow_id'));
            }
            if(isset($_POST['unfollowUser'])){
                $this->followers_model->removeFollowing($_SESSION['user_id'],$this->input->post('unfollow_id'));
            }
            $following = $this->followers_model->getFollowing($_SESSION['user_id']);
        }

        $bugtype = (array) $this->survey_model->getBugtypeByName($user_info->bugtype);


        $user_photos = $this->user_photos_model->getAllPhotos($user_info->id);

        //pagination settings
        $config['base_url'] = base_url('user_page/user/'.$username.'/');
        $config['total_rows'] = count($user_photos);
        $config['per_page'] = "30";
        $config["uri_segment"] = 4;
        $config["num_links"] = 2;

        // integrate bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '«';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $paginatedFiles = 0;
        if (count($user_photos) > 0) {
            $paginatedFiles = array_slice($user_photos, $page, $config['per_page'], true);
        }

        $pagination = $this->pagination->create_links();

        $pagevars = array(
            'active_menu'      => $page_title,
            'user_info'        => $user_info,
            'profile_pic_path' => $profile_pic_path,
            'following'        => $following,
            'bugtype'          => isset($bugtype[0]) ? $bugtype[0] : '',
            'user_photos'      => $paginatedFiles,
            'pagination'       => $pagination,
        );

        $this->load->view('header',$pagevars);
        $this->load->view('user_page/user_page');
        $this->load->view('footer');
    }
}
