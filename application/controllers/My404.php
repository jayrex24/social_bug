<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class my404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_settings_model'));
        $this->load->helper(array('form', 'url','socialbug'));
        $this->load->library(array('session','form_validation','email'));
    }

    public function index()
    {
        $page = 'Page Not Found';

        $pagevars = array(
            'active_menu' => $page
        );

        $this->output->set_status_header('404');
        $this->load->view('header', $pagevars);
        $this->load->view('errors/not_found');
        $this->load->view('footer');
    }
}
