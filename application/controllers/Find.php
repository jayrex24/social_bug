<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Find extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','followers_model', 'survey_model', 'user_auth_model', 'find_model', 'user_photos_model', 'followers_model'));
        $this->load->helper(array('form', 'url', 'socialbug'));
        $this->load->library(array('session', 'form_validation', 'pagination'));
    }

    public function user_matches(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'User Matches';
        $this->success = null;
        $this->errors = null;

        $matches = null;
        $following = null;

        $id         = $this->session->all_userdata();
        $user_info  = $this->user_model->get_user($id['user_id']);

        if(!empty($user_info->bugtype)){
            $opposites = $this->getResults($user_info->bugtype);
            $matches   = $this->find_model->getUserMatches($user_info->id, $opposites);

            $following = getFollower($_SESSION['user_id'], $this->input->post('follow_id'), $this->input->post('unfollow_id'));

            $config = paginationSettings(base_url('find/user_matches'), count($matches), 12, 3);
            $this->pagination->initialize($config);

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            $paginatedFiles = 0;
            if (count($matches) > 0) {
                $paginatedFiles = array_slice($matches, $page, $config['per_page'], true);
            }

            $pagination = $this->pagination->create_links();

        }else{
            $this->session->set_flashdata('message', 'Find out who you closely match, take the personality test. It\'s super easy.');
            redirect(base_url('user_survey/take_survey'));
        }

        $pagevars = array(
            'active_menu' => $page_title,
            'matches'     => $paginatedFiles,
            'following'   => $following,
            'pagination'  => $pagination,
        );

        $this->load->view('header', $pagevars);
        $this->load->view('find/user_matches');
        $this->load->view('footer');
    }

    public function user_opposites(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'User Opposites';
        $this->success = null;
        $this->errors = null;

        $matches = null;
        $following = null;

        $id         = $this->session->all_userdata();
        $user_info  = $this->user_model->get_user($id['user_id']);

        if(!empty($user_info->bugtype)){
            $opposites = $this->getResults($user_info->bugtype);
            $matches   = $this->find_model->getUserOpposites($user_info->id, $opposites);

            $following = getFollower($_SESSION['user_id'], $this->input->post('follow_id'), $this->input->post('unfollow_id'));

            $config = paginationSettings(base_url('find/user_opposites'), count($matches), 12, 3);
            $this->pagination->initialize($config);

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            $paginatedFiles = 0;
            if (count($matches) > 0) {
                $paginatedFiles = array_slice($matches, $page, $config['per_page'], true);
            }

            $pagination = $this->pagination->create_links();

        }else{
            $this->session->set_flashdata('message', 'Want to know who has a different personality compared to yours? Take the personality test. It\'s super easy.');
            redirect(base_url('user_survey/take_survey'));
        }

        $pagevars = array(
            'active_menu' => $page_title,
            'matches'     => $paginatedFiles,
            'following'   => $following,
            'pagination'  => $pagination,
        );

        $this->load->view('header', $pagevars);
        $this->load->view('find/user_opposites');
        $this->load->view('footer');
    }

    private function getResults($user_bugtype){
        $opposite = null;

        switch ($user_bugtype){
            case 'Fire Ant':
            case 'Cockroach':
            case 'Bumble Bee':
            case 'Grasshopper':
            case 'Hercules Beetle':
            case 'Daddy Longlegs':
            case 'Dragonfly':
            case 'Cricket':
                $opposite = array(
                    'Praying Mantis',
                    'Monarch Butterfly',
                    'Wasp',
                    'Luna Moth',
                    'Lady Bug',
                    'Black Widow',
                    'Fly',
                    'Walkingstick',
                );
                break;
            case 'Fly':
            case 'Praying Mantis':
            case 'Monarch Butterfly':
            case 'Wasp':
            case 'Luna Moth':
            case 'Lady Bug':
            case 'Black Widow':
            case 'Walkingstick':
                $opposite = array(
                    'Cockroach',
                    'Bumble Bee',
                    'Grasshopper',
                    'Hercules Beetle',
                    'Daddy Longlegs',
                    'Dragonfly',
                    'Fire Ant',
                    'Cricket',
                );
                break;
            default:
                break;
        }

        return $opposite;
    }
}