<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Socialize extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_settings_model','sendmail_model','socialize_model','user_photos_model', 'followers_model'));
        $this->load->helper(array('form', 'url','socialbug','states'));
        $this->load->library(array('session', 'form_validation', 'email', 'pagination'));
        $this->load->database();
    }

    public function index() {
        $page_title = 'Socialize';
        $this->success = null;
        $this->errors  = null;
        $following = null;
        $username = null;

        //get users followers
        if(isset($_SESSION['logged_in'])){
            $following = getFollower($_SESSION['user_id'], $this->input->post('follow_id'), $this->input->post('unfollow_id'));
            $username  = $_SESSION['username'];
        }

        $all_users = $this->socialize_model->getAllUsers($username);

        $config = paginationSettings(base_url('socialize/index'), count($all_users), 12, 3);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $paginatedFiles = 0;
        if (count($all_users) > 0) {
            $paginatedFiles = array_slice($all_users, $page, $config['per_page'], true);
        }

        $pagination = $this->pagination->create_links();

        $pagevars = array(
            'active_menu' => $page_title,
            'success'     => $this->success,
            'errors'      => $this->errors,
            'userlist'    => $paginatedFiles,
            'pagination'  => $pagination,
            'following'   => $following,
        );

        $this->load->view('header', $pagevars);
        $this->load->view('socialize/socialize');
        $this->load->view('footer');
    }

    function search() {
        $page_title = 'Socialize';
        $this->success = null;
        $this->errors  = null;
        $following = null;

        // get search string
        $search = ($this->input->post("search_bar"))? $this->input->post("search_bar") : "NIL";
        $search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

        $config = paginationSettings(base_url("socialize/search/{$search}"), $this->socialize_model->get_users_count($search), 12, 4);
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $binds = array(
            'limit' => $config['per_page'],
            'start' => $page,
            'st'    => $search,
        );

        // get users list
        $userlist = $this->socialize_model->get_users($binds);

        $pagination = $this->pagination->create_links();

        if(isset($_SESSION['logged_in'])) {
            $following = getFollower($_SESSION['user_id'], $this->input->post('follow_id'), $this->input->post('unfollow_id'));
        }

        $pagevars = array(
            'active_menu' => $page_title,
            'success'     => $this->success,
            'errors'      => $this->errors,
            'userlist'    => $userlist,
            'pagination'  => $pagination,
            'page'        => $page,
            'following'   => $following,
            'search_bar'  => $search,
        );

        $this->load->view('header', $pagevars);
        $this->load->view('socialize/socialize');
        $this->load->view('footer');
    }
}