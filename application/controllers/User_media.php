<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_media extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('user_model','user_settings_model','user_photos_model','followers_model'));
        $this->load->model(array('user_auth_model','user_sidebar_model'));
        $this->load->helper(array('form', 'url','socialbug','states'));
        $this->load->library(array('session','form_validation','email','pagination'));
    }

    // Upload user photos
    public function upload_photos(){
        $this->user_auth_model->is_logged_out();
        $page = 'Upload Photos';
        $this->success = null;
        $this->errors = null;

        $data = array();

        if (!file_exists('assets/user_photo_uploads/' . $_SESSION['username'] . '_pictures')) {
            mkdir('assets/user_photo_uploads/' . $_SESSION['username'] . '_pictures', 0777, true);
        }

        if ($this->input->post('upload_photos')) {
            $dir = "assets/user_photo_uploads/" . $_SESSION['username'] . "_pictures/";
            //set the path where the files uploaded will be copied. NOTE if using linux, set the folder to permission 777
            $config['upload_path'] = $dir;

            // set the filter image types
            $config['allowed_types'] = 'gif|jpg|png';

            //load the upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $data['upload_data'] = '';

            if (!$this->upload->do_upload('userfile')) {
                $this->errors[] = $this->upload->display_errors();
            } else {
                $upload_data = $this->upload->data();
                $this->success[] = sprintf("Your file <strong>%s</strong> has been successfully uploaded. <a href='%s'>View Here</a> ", $upload_data['file_name'], base_url('user_media/view_gallery'));

                $image_data = array(
                    'user_id'     => $_SESSION['user_id'],
                    'location'    => base_url($dir.$upload_data['file_name']),
                    'image_name'  => $upload_data['file_name'],
                    'description' => '',
                    'image_type'  => $upload_data['file_type'],
                    'set_profile' => 0,
                );

                $this->user_photos_model->insertPhotoInfo($image_data);
            }
        }

        $pagevars = array(
            'active_menu' => $page,
            'success'     => $this->success,
            'errors'      => $this->errors,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_media/user_upload_photos');
        $this->load->view('footer');
    }

    public function edit_photos(){
        $this->user_auth_model->is_logged_out();
        $page = 'Edit Photos';
        $this->success = null;
        $this->errors = null;
        $description = null;
        $set_profile = null;

        $user_id = $this->uri->segment(3);
        $photo = $this->uri->segment(4);

        $selected_photo = $this->user_photos_model->getPhoto($user_id, $photo);

        if ($this->input->post('editphoto')) {

            if ($this->input->post('image_desc')) {
                $this->success[] = 'Image description has been updated';
                $description = $this->input->post('image_desc');
            }

            if ($this->input->post('set_profile')) {
                $this->success[] = sprintf('<strong>%s</strong> is now your profile photo.', $this->input->post('image_name'));
                $set_profile = $this->input->post('set_profile');
                $this->user_photos_model->removeProfile($user_id);
            }

            $binds = array(
                'user_info' => array(
                    'user_id' => $_SESSION['user_id'],
                    'image_name' => $selected_photo[0]['image_name'],
                ),
                'update_info' => array(
                    'description' => $description,
                    'set_profile' => $set_profile,
                ),
            );

            $this->user_photos_model->updatePhotos($binds);
            $selected_photo = $this->user_photos_model->getPhoto($user_id, $photo);

        } elseif (isset($_POST['deletePhoto'])) {
            if ($this->input->post('curr_profile_pic')) {
                $this->errors[] = sprintf('<strong>%s</strong> is currently your profile photo, cannot process deletion.', $this->input->post('image_name'));
            } else {
                $this->load->helper("file");
                unlink($selected_photo[0]['location']);
                $this->user_photos_model->deletePhoto($selected_photo[0]['id']);
                redirect('user_media/view_gallery');
            }
        }

        $pagevars = array(
            'active_menu'    => $page,
            'success'        => $this->success,
            'errors'         => $this->errors,
            'selected_photo' => $selected_photo[0],
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_media/edit_photo');
        $this->load->view('footer');
    }

    // View user photos
    public function view_gallery(){
        $this->user_auth_model->is_logged_out();
        $page_title = 'View Gallery';
        $this->success = null;
        $this->errors = null;

        $user_photos = $this->user_photos_model->getAllPhotos($_SESSION['user_id']);

        //pagination settings
        $config['base_url'] = base_url('user_media/view_gallery');
        $config['total_rows'] = count($user_photos);
        $config['per_page'] = "60";
        $config["uri_segment"] = 3;
        $config["num_links"] = 2;

        // integrate bootstrap pagination
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '«';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $paginatedFiles = 0;
        if (count($user_photos) > 0) {
            $paginatedFiles = array_slice($user_photos, $page, $config['per_page'], true);
        }

        $pagination = $this->pagination->create_links();

        $pagevars = array(
            'active_menu' => $page_title,
            'success'     => $this->success,
            'errors'      => $this->errors,
            'user_photos' => $paginatedFiles,
            'pagination'  => $pagination,
        );

        $this->load->view('header', $pagevars);
        $this->user_sidebar_model->getSidebar();
        $this->load->view('user_media/view_gallery');
        $this->load->view('footer');
    }
}