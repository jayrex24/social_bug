<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class User_Model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function create_user($username, $email, $password) {
		
		$data = array(
			'username'   => $username,
			'email'      => $email,
			'password'   => $this->hash_password($password),
			'created_at' => date('Y-m-j H:i:s'),
		);
		
		return $this->db->insert('users', $data);
		
	}

    public function trackingInfo($binds){

        $date = date('Y-m-d H:i:s');

        $data = array(
            'user_id'    => $binds['user_id'],
            'ip_address' => $binds['ip_address'],
            'timestamp'  => $date,
        );

        $this->db->insert('ci_sessions', $data);

        return true;

    }
	
	/**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($username, $password) {
		
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('username', $username);
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
		
	}

	public function get_user_id_from_username($username) {
		
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('username', $username);

		return $this->db->get()->row('id');
	}

    public function get_user_info_from_username($username) {

        $this->db->from('users');
        $this->db->where('username', $username);

        return $this->db->get()->row();
    }

	public function get_user($user_id) {
		
		$this->db->from('users');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
	}
	
	/**
	 * 
	 * @access public
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
    public function hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}

	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
    private function verify_password_hash($password, $hash) {
		return password_verify($password, $hash);
	}

    public function getUserInfoByEmail($email){
        $q = $this->db->get_where('users', array('email' => $email), 1);
        if($this->db->affected_rows() > 0){
            $row = $q->row();
            return $row;
        }else{
            error_log('no user found getUserInfo('.$email.')');
            return false;
        }
    }

	public function getUserInfo($id){
		$q = $this->db->get_where('users', array('id' => $id), 1);
		if($this->db->affected_rows() > 0){
			$row = $q->row();
			return $row;
		}else{
			error_log('no user found getUserInfo('.$id.')');
			return false;
		}
	}

    public function insertToken($user_id){
        $token = substr(sha1(rand()), 0, 30);
        $date = date('Y-m-d');

        $string = array(
            'token'=> $token,
            'user_id'=>$user_id,
            'created'=>$date
        );
        $query = $this->db->insert_string('tokens',$string);
        $this->db->query($query);
        return $token . $user_id;

    }

    public function removeToken($user_id){

        $this->db->where('user_id', $user_id);
        $this->db->delete('tokens');

        return true;
    }

    public function isTokenValid($token){
        $tkn = substr($token,0,30);
        $uid = substr($token,30);

        $q = $this->db->get_where(
            'tokens', array(
                'tokens.token'   => $tkn,
                'tokens.user_id' => $uid
            ), 1
        );

        if($this->db->affected_rows() > 0){
            $row = $q->row();

            $created = $row->created;
            $createdTS = strtotime($created);
            $today = date('Y-m-d');
            $todayTS = strtotime($today);

            if($createdTS != $todayTS){
                return false;
            }

            $user_info = $this->getUserInfo($row->user_id);
            return $user_info;

        }else{
            return false;
        }

    }
}
