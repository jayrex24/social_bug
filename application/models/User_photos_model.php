<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User_Photos_Model extends CI_Model {

    public function insertPhotoInfo($binds){

        $sql = "INSERT INTO user_images
                SET user_id     = {$binds['user_id']},
                    location    = '{$binds['location']}',
                    image_name  = '{$binds['image_name']}',
                    description = '{$binds['description']}',
                    image_type  = '{$binds['image_type']}',
                    set_profile = {$binds['set_profile']}";

        return $this->db->query($sql);
    }

    public function getAllPhotos($user_id){

        $sql = "SELECT * 
                  FROM user_images 
                 WHERE user_id = {$user_id}";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getPhoto($user_id, $photo_name){

        $sql = "SELECT * 
                  FROM user_images 
                 WHERE user_id = {$user_id}
                   AND image_name = '{$photo_name}'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function updatePhotos($binds){
        $set = array();
        foreach ($binds['update_info'] as $k => $v){
            if(!empty($v)){
                $set[] = $k." = '".$v."'";
            }
        }
        $set_implode = implode(', ', $set);

        if(!empty($set_implode)){
            $sql = "UPDATE user_images
                       SET {$set_implode}
                     WHERE user_id = {$binds['user_info']['user_id']}
                       AND image_name = '{$binds['user_info']['image_name']}'";

            return $this->db->query($sql);
        }else{
            return false;
        }
    }

    public function removeProfile($user_id){

        $sql = "UPDATE user_images
                   SET set_profile = 0
                 WHERE user_id = {$user_id}";

        return $this->db->query($sql);
    }

    public function getProfilePic($user_id){

        $sql = "SELECT * 
                  FROM user_images
                 WHERE user_id = {$user_id}
                   AND set_profile = 1";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function deletePhoto($id){

        $sql = "DELETE 
                  FROM user_images
                 WHERE id = {$id}";

        return $this->db->query($sql);
    }
}