<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SendEmail class.
 *
 * @extends CI_Controller
 */
class Sendmail_Model extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->library('email'); // load the library
    }

    public function sendEmail($email_type, $user_email, $username, $message = ""){
        // Email configuration
        $config = Array(
            'protocol'  => 'sendmail',
            'smtp_host' => 'smtp.jaysonmalabanan.com',
            'smtp_port' => 465,
            'smtp_user' => 'webmaster@jaysonmalabanan.com',
            'smtp_pass' => '-K(D7sn3NE9yjvrazD',
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
            'wordwrap'  => TRUE
        );

        Switch($email_type){
            case "New User":
                $this->load->library('email', $config);
                $this->email->from($config['smtp_user'], "Social Bug Admin");
                $this->email->to($user_email);
                $this->email->subject(sprintf(_("Welcome to Social Bug %s"), $username));
                $this->email->message(sprintf(_("Hello %s, \r\n\r\nWe're excited to have you. Please feel free to poke around Social Bug. \r\n\r\nThanks,\r\nSocial Bug Admin"),$username));
                $this->email->send();
                break;
            case "Password Reset":
                $this->load->library('email', $config);
                $this->email->from($config['smtp_user'], "Social Bug Admin");
                $this->email->to($user_email);
                $this->email->subject("Social Bug Password Reset");
                $this->email->message($message);
                $this->email->send();
                break;
            default:
                break;
        }

        return;
    }
}
