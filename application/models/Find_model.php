<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SendEmail class.
 *
 * @extends CI_Controller
 */
class Find_Model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function getUserMatches($user_id, $opposites_array){

        $set = array();
        foreach ($opposites_array as $k => $v){
            if(!empty($v)){
                $set[] = "'".$v."'";
            }
        }
        $list = implode(', ', $set);

        $sql = "SELECT * 
                  FROM users 
                 WHERE bugtype NOT IN ({$list})
                   AND id != $user_id";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function getUserOpposites($user_id, $opposites_array){

        $set = array();
        foreach ($opposites_array as $k => $v){
            if(!empty($v)){
                $set[] = "'".$v."'";
            }
        }
        $list = implode(', ', $set);

        $sql = "SELECT * 
                  FROM users 
                 WHERE bugtype IN ({$list})
                   AND id != $user_id";

        $query = $this->db->query($sql);

        return $query->result_array();
    }
}
