<?php
/**
 * Created by PhpStorm.
 * User: Jayson
 * Date: 11/26/2016
 * Time: 8:45 PM
 */

class User_Sidebar_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->model(array('user_photos_model', 'mahana_model'));
    }

    public function getSidebar(){
        $data = array();

        $thread = $this->mahana_model->get_all_threads($_SESSION['user_id']);
        $message_unread = 0;
        foreach ($thread as $th){
            if($th['user_name'] != $_SESSION['username']){
                if($th['status'] == 0){
                    $message_unread++;
                }
            }
        }
        $data['message_unread'] = $message_unread;

        $profile_pic = $this->user_photos_model->getProfilePic($_SESSION['user_id']);
        if(!empty($profile_pic)){
            $data['profile_pic'] = $profile_pic[0]['location'];
        }else{
            $data['profile_pic'] = "/assets/images/default.png";
        }

        $this->load->view('user_sidebar/user_sidebar', $data);
    }
}
