<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Socialize_Model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function get_users($binds) {
        if ($binds['st'] == "NIL") $binds['st'] = "";

        $curr_user = isset($_SESSION['username']) ? $_SESSION['username'] : '' ;
        if(!empty($binds['st'])){
            $curr_user = "";
        }

        $sql = "SELECT * 
                  FROM users 
                 WHERE username != '{$curr_user}'
                   AND (username LIKE '%{$binds['st']}%' OR CONCAT(first_name,' ',last_name) LIKE '%{$binds['st']}%')
                 LIMIT {$binds['start']}, {$binds['limit']} ";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function getAllUsers($username = null) {
        $this->db->where('username != ', $username);
        $query = $this->db->get('users');
        $result = $query->result_array();
        return $result;
    }

    function get_users_count($st = NULL)
    {
        if ($st == "NIL") $st = "";

        $curr_user = isset($_SESSION['username']) ? $_SESSION['username'] : '' ;

        $sql = "SELECT * 
                  FROM users 
                 WHERE username != '{$curr_user}'
                   AND (username LIKE '%{$st}%' OR CONCAT(first_name,' ',last_name) LIKE '%{$st}%')";

        $query = $this->db->query($sql);
        return $query->num_rows();
    }
}