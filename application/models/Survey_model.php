<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey_Model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function getQuestions() {

        $sql = "SELECT * 
                  FROM survey";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    public function updateBugtype($user_id, $bugtype){

        $sql = "UPDATE users
                   SET bugtype = '{$bugtype}'
                 WHERE id = {$user_id}";

        $this->db->query($sql);

        return true;
    }

    public function getBugtypeByName($bugtype) {

        $sql = "SELECT * 
                  FROM bugtype
                 WHERE bugtype ='{$bugtype}'";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }
}