<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Followers_Model extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    public function getFollowing($user_id) {

        $sql = "SELECT following_id 
                  FROM followers 
                 WHERE user_id = {$user_id}";

        $query = $this->db->query($sql);

        $result = $query->result_array();

        $followerList = array();
        foreach($result as $res){
            $followerList[] = $res['following_id'];
        }

        return $followerList;
    }

    public function addFollowing($curr_user_id, $following_user_id) {

        $search = "SELECT *
                     FROM followers
                    WHERE user_id = {$curr_user_id}
                      AND following_id = {$following_user_id}";

        $query = $this->db->query($search);

        if(count($query->result_array()) > 0){
            return false;
        }else{
            $sql = "INSERT INTO followers
                SET user_id      = {$curr_user_id},
                    following_id = {$following_user_id}";

            return $this->db->query($sql);
        }
    }

    public function removeFollowing($curr_user_id, $following_user_id) {

        $sql = "DELETE 
                  FROM followers
                 WHERE user_id = {$curr_user_id}
                   AND following_id = {$following_user_id}";

        return $this->db->query($sql);
    }

    public function getFollowers($follower_id) {

        $sql = "SELECT user_id 
                  FROM followers 
                 WHERE following_id = {$follower_id}";

        $query = $this->db->query($sql);

        $result = $query->result_array();

        $followerList = array();
        foreach($result as $res){
            $followerList[] = $res['user_id'];
        }

        return $followerList;
    }
}