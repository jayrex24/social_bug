<?php
/**
 * Created by PhpStorm.
 * User: Jayson
 * Date: 11/26/2016
 * Time: 8:41 PM
 */

class User_Auth_Model extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->library(array('session'));
    }

    public function is_logged_out(){
        if (isset($_SESSION['logged_in']) == false) {
            redirect('user/login');
        }
    }

    public function is_logged_in(){
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == true) {
            redirect(base_url());
        }
    }
}