<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class User_Settings_Model extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

    /**
     * create_user function.
     *
     * @access public
     * @param mixed $binds
     * @return bool true on success, false on failure
     */
    public function update_information($binds, $id) {

        $userInfoArray = array();

        if(isset($binds['first_name'])){
            $userInfoArray['first_name'] = $binds['first_name'];
        }
        if(isset($binds['last_name'])){
            $userInfoArray['last_name'] = $binds['last_name'];
        }
        if(isset($binds['birth_date'])){
            $userInfoArray['birth_date'] = $binds['birth_date'];
        }
        if(isset($binds['gender'])){
            $userInfoArray['gender'] = $binds['gender'];
        }
        if(isset($binds['address'])){
            $userInfoArray['address'] = $binds['address'];
        }
        if(isset($binds['city'])){
            $userInfoArray['city'] = $binds['city'];
        }
        if(isset($binds['state'])){
            $userInfoArray['state'] = $binds['state'];
        }
        if(isset($binds['zip'])){
            $userInfoArray['zip'] = $binds['zip'];
        }

        $this->db->where('id', $id['user_id']);
        $this->db->update('users', $userInfoArray);

        return true;
    }


    public function update_about($binds, $id) {

        $userInfoArray = array();

        if(isset($binds['interests'])){
            $userInfoArray['interests'] = $binds['interests'];
        }
        if(isset($binds['favorite_movies'])){
            $userInfoArray['favorite_movies'] = $binds['favorite_movies'];
        }
        if(isset($binds['favorite_books'])){
            $userInfoArray['favorite_books'] = $binds['favorite_books'];
        }

        $this->db->where('id', $id['user_id']);
        $this->db->update('users', $userInfoArray);

        return true;
    }

    public function update_password($password, $id) {

        $userInfoArray = array();

        if(isset($password)){
            $userInfoArray['password'] = $password;
        }

        $this->db->where('id', $id);
        $this->db->update('users', $userInfoArray);

        return true;
    }
}
