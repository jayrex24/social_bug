<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Random_User_Model extends CI_Model{


    public function __construct() {
        parent::__construct();
        $this->load->database();
    }


    public function getAllBugType(){

        $sql = "SELECT bugtype 
                  FROM bugtype";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        $arr = array();
        foreach($result as $res){
            $arr[] = $res['bugtype'];
        }

        return $arr;
    }

    public function insertGeneratedUser($binds){

        $this->db->insert('users', $binds);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function insertProfilePhoto($binds){

        $this->db->insert('user_images', $binds);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

}
