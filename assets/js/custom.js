function validate(){
	var value = Number(document.getElementById("zip").value);
	if (Math.floor(value) != value) {
	  alert("Zipcode must contain integers ex. 12345");
	  $("input[type=submit]").prop("disabled", false);
	}

    if ($('#first_name').val().length > 0
    	|| $('#last_name').val().length > 0
    	|| $('#birth_date').val().length > 0
    	|| $('#address').val().length > 0
        || $('#city').val().length > 0
        || $('#city').val().length > 0
        || $('#state').val().length > 0
        || $('#zip').val().length > 0) {
        $("input[type=submit]").prop("disabled", false);
    } else {
        $("input[type=submit]").prop("disabled", true);
	}
}

function showModal(){
	// Get the modal
	var modal = document.getElementById('myModal');

	modal.addEventListener('click',function(){
		this.style.display="none";
	})

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		modal.style.display = "none";
	}

	// Get all images and insert the clicked image inside the modal
	// Get the content of the image description and insert it inside the modal image caption
	var images = document.getElementsByTagName('img');
	var modalImg = document.getElementById("img01");
	var captionText = document.getElementById("caption");
	var i;
	for (i = 0; i < images.length; i++) {
		images[i].onclick = function() {
			modal.style.display = "block";
			modalImg.src = this.src;
			modalImg.alt = this.alt;
			captionText.innerHTML = this.nextElementSibling.innerHTML;
		}
	}
}

function searchClear() {
	$("#search_bar").val('');
	window.location = "/socialize";
}