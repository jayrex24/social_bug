<VirtualHost *:80>

        DocumentRoot /var/www/projects/social_bug
        ServerName socialbug.jaysonmalabanan.vm
        ServerAlias socialbug.jaysonmalabanan.vm

        <Directory /var/www/projects/social_bug>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

</VirtualHost>
